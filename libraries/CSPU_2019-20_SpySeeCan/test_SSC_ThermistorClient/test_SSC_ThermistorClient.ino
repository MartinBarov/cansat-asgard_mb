/*
   test_SSC_ThermistorClient
*/

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "SSC_ThermistorClient.h"
#include "SSC_BMP280_Client.h"

constexpr bool csvFormat = true;

SSC_ThermistorClient therm;
SSC_BMP280_Client bmp;
SSC_Record record;

void setup() {
  // put your setup code here, to run once:
  DINIT(115200);
  if (csvFormat) {
    Serial << "time (seconds),BMP280, NTCLE100E3, NTCLG100E2, VMA320" << ENDL;
  }
  bmp.begin(1037.6);
  Serial << "BMP OK"<< ENDL;
}

void loop() {
  record.clear();
  
  bool result = therm.readData(record);
  if (!result) {
    Serial << "*** error while reading temperature of the thermistors" << ENDL;
  }
  result = bmp.readData(record);
  if (!result) {
    Serial << "*** error while reading temperature of the bmp" << ENDL;
  }

  if (csvFormat) {
    Serial << millis() / 1000.0 << "," << record.temperatureBMP << " ," << record.temperatureThermistor1 << " ," << record.temperatureThermistor2 << " , " << record.temperatureThermistor3 << ENDL;
  }
  else
    record.print(Serial);

  delay(5000);

}
