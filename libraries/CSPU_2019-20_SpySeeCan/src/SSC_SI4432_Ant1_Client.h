/**
 * SSC_SI4432_Ant1_Client.h 
 */

#pragma once
#include "SI4432_Client.h"
/**
 * @ingroup SpySeeCanCSPU
 * @brief This class takes care of the configuration of the SI4432 module. It can provide the RSSI (Received Signal Strength Intensity)on one or multiples frequencies (in dBm).
 * It is an instance for the SpySeeCan project.
 */
class SSC_SI4432_Ant1_Client : public SI4432_Client
{
public:
    SSC_SI4432_Ant1_Client(uint8_t theSlaveSelectPin) : SI4432_Client(theSlaveSelectPin) {}
    SSC_SI4432_Ant1_Client(uint8_t theSlaveSelectPin, uint8_t theInterruptPin) : SI4432_Client(theSlaveSelectPin, theInterruptPin) {}

    float getMinAttFrequency() const
    {
        return minAttFrequency;
    }
    float getMaxAttFrequency() const
    {
        return maxAttFrequency;
    }
    byte getNumStepFreq() const
    {
        return numStepFreq;
    }
    byte getAttenuationSize() const
    {
        return attenuationSize;
    }
    float getAttStepFrequency() const
    {
        return attStepFrequency;
    }
    const byte *const getAttenuationOffset() const
    {
        return attenuationOffset;
    }

protected:
    static constexpr float minAttFrequency = 433.00;
    static constexpr float maxAttFrequency = 868.00;
    static constexpr byte numStepFreq = 88;
    static constexpr byte attenuationSize = numStepFreq + 1;

    static constexpr float attStepFrequency = (maxAttFrequency - minAttFrequency) / numStepFreq;
    static constexpr byte attenuationOffset[attenuationSize] = {
        1, 1, 1, 1, 1,
        1, 1, 7, 11, 4,
        11, 10, 10, 11, 4,
        5, 5, 5, 5, 6,
        7, 9, 10, 10, 11,
        11, 12, 12, 13, 13,
        13, 13, 13, 13, 13,
        13, 13, 13, 13, 13,
        13, 13, 13, 13, 13,
        12, 12, 11, 11, 11,
        12, 13, 13, 13, 14,
        15, 16, 17, 18, 19,
        20, 21, 22, 23, 24,
        24, 25, 25, 26, 26,
        27, 27, 27, 28, 28,
        29, 31, 32, 32, 32,
        32, 32, 32, 32, 32,
        32, 32, 32, 32};
};