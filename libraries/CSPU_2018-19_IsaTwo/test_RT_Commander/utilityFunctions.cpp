
#include "Arduino.h" 
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "IsaTwoInterface.h"

extern int numErrors;

void requestVisualCheck(const char* msg, bool separatorFirst=true) {                 /**<Method used to request a visual check after an action.*/
  char answer = ' ';
  if (separatorFirst) {
    Serial << ENDL;
  }
  while (Serial.available() > 0) {
    Serial.read();
  }

  while ((answer != 'y') && (answer != 'n')) {
    Serial << ENDL << msg << F(". Is this ok(y/n) ? ");
    Serial.flush();
    while (Serial.available() == 0) {
      delay(300);
    }
    answer = Serial.read();
  }
  if (answer == 'n') {
    numErrors++;
  }
  Serial << ENDL;
}

void requestVisualCheckWithCmd(String &theCmd, const char* msg) {
  Serial << "Sent command '" << theCmd << "'" << ENDL;
  requestVisualCheck(msg, false); 
}

/* Build a request string with 0, 1 or 2 parameters (parameters are ignored if they are empty strings).  */
void buildRequest(String &theCmd, IsaTwoCmdRequestType reqType, const char* param1="", const char* param2="") {
  theCmd=(int) IsaTwoRecordType::CmdRequest;
  theCmd+= ',';
  theCmd += (int) reqType;
  if (strlen(param1) >0) {
    theCmd += ',';
    theCmd += param1;
  }
  if (strlen(param2) >0) {
    theCmd += ',';
    theCmd += param2;
  }
  Serial << "Command built for test: '" << theCmd << "'" <<ENDL;
}
