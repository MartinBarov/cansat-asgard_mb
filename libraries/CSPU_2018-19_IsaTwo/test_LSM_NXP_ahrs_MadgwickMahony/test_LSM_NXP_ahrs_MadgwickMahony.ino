/*
   This is the ahrs_mahony sketch from adafruit, modified for LSM9DS0 and NXP Precision

   WARNING: This sketch expects the magnetometers calibration parameters have been obtained using MotionCal
            and the calib_MagWithMotionCal sketch (data obtained with MagMaster could require a different usage). 

   Be sure to select the sensor and the filter algorithms below.
   
*/

// --------- Sketch configuration --------
constexpr uint8_t AcquisitionPeriod = 10; // msec (Cannot be less than 10 msec.)
constexpr uint16_t OutputPeriod = 100; // msec
//#define USE_NXP_IMU        // Define to use NXP imu, undefine to use LSM.
//define USE_MADGWICK      // Define to use Madgwick, undefine to use Mahony.
#define USE_MAGNETOMETERS  // Define to use the update with 9-DOF, undefine to use the updateIMU
                           // (no use of magnetometers, for debugging only).
//define HUMAN_READABLE_OUTPUT   // Define for human-readable output, undefine to generate ground records. 
#define ACTUAL_CALIBRATION_DATA // Define to use actual calibration data, otherwise unity matrix and 0 offsets are used.
#define CALIBRATE_GYROS // Define to have gyro zero-rates corrected (only effectif if ACTUAL_CALIBRATION_DATA is defined
                          // NB: Gyro calibration is not implemented for NXP (apparently not required).
//#define PRINT_CALIBRATED_DATA // If defined print the calibration magnetic data (for debugging). 
constexpr bool ForceGyrosToZero=false; // For debugging.
#define USE_I2C     // Define to communicate by I2C, otherwise SPI is used. SPI NOT SUPPORTED YET. 

// --------- Wiring config for SPI -----------------
constexpr byte GyroCS=10;
constexpr byte AccelMag_CS=10;

// --------- end of configuration ----------
#include <Wire.h>
#ifdef USE_NXP_IMU
#include <Adafruit_FXAS21002C.h>
#include <Adafruit_FXOS8700.h>
#else
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM9DS0.h>
#endif

#include <MahonyAHRS.h>
#ifdef USE_MADGWICK
#include <MadgwickAHRS.h>
#else
#include <MahonyAHRS.h>
#endif

#include "elapsedMillis.h"
#include "DebugCSPU.h"

// Note: This sketch requires the MahonyAHRS sketch from
// https://github.com/PaulStoffregen/MahonyAHRS, or the
// MagdwickAHRS sketch from https://github.com/PaulStoffregen/MadgwickAHRS

// Note: This sketch is a WORK IN PROGRESS

// Create sensor instances.
#ifdef USE_NXP_IMU
Adafruit_FXAS21002C gyro = Adafruit_FXAS21002C(0x0021002C);
Adafruit_FXOS8700 accelmag = Adafruit_FXOS8700(0x8700A, 0x8700B);
#else
#  ifdef USE_I2C
  Adafruit_LSM9DS0 lsm = Adafruit_LSM9DS0(1000);  // Use I2C, ID #1000
#  else
  Adafruit_LSM9DS0 ( int8_t clk, int8_t miso, int8_t mosi, int8_t xmcs, int8_t gcs, int32_t sensorID = 0 );
#  endif
#endif

// Mag calibration values are calculated via:
// - calib_IMU_AccelGyro for the LSM gyro offsets.
//      !! See note below about NXP gyro calibration! 
// - MotionCal or MagMaster for the magnetometers offset and transformation matrix. 

#ifdef ACTUAL_CALIBRATION_DATA

#  ifdef USE_NXP_IMU
   const float GyroZeroRate[3] = {0.0f,0.0f,0.0f }; // WARNING: using non-null gyro offset is not 
                                                    //          implemented yet for NXP. 
   const float mag_offsets[3]  = { 50.60f,67.49f,70.77f};
    
    // Soft iron error compensation matrix
    const float mag_softiron_matrix[3][3] = { 
      { 0.987f , -0.036f, 0.015f},
      { -0.036f, 0.983f , 0.004f },
      {-0.015f, 0.004f, 1.033f }
    };
#  else
    const float GyroZeroRate[3]= {-139.08000183f,-48.06000137f,-414.05999756f};
    // This is calibrated using MotionCal.
    const float mag_offsets[3]            = { -1.47f, -7.77f, -11.29f};
    
    // Soft iron error compensation matrix
    const float mag_softiron_matrix[3][3] = { 
      { 0.986f , -0.032f, 0.006f},
      { -0.032f, 0.938f , 0.005f },
      {0.006f, 0.005f, 1.082f }
    };
#  endif
#else
    const float GyroZeroRate[3]= {0.0f, 0.0f, 0.0f};
    float mag_offsets[3]            = { 0.0f,0.0f,0.0f};
    
    // Soft iron error compensation matrix
    float mag_softiron_matrix[3][3] = { { 1.0f, 00.0f, 0.0f},
      { 0.0f, 1.0f, 0.0f},
      { 0.0f, 0.0f, 1.0f }
    };
#endif

// Mahony is lighter weight as a filter and should be used
// on slower systems
#ifdef USE_MADGWICK
Madgwick filter;
#else
Mahony filter;
#endif


elapsedMillis elapsed, elapsedDisplay;
unsigned long timestamp, previousTimestamp;

void configureSensor(void)
{
#ifdef USE_NXP_IMU
  // No configuration
#else
  lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
  lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
#endif
}

void setup()
{
  Serial.begin(115200);

  // Wait for the Serial Monitor to open (comment out to run without Serial Monitor)
  while (!Serial);

  Serial.println(F("Adafruit 9 DOF Board AHRS Example")); Serial.println("");

#ifndef ACTUAL_CALIBRATION_DATA
  Serial << " 'Calibrating' with unity matrix and 0 offsets" << ENDL;
#endif

  // Initialize the sensors.
#ifdef USE_NXP_IMU
  Serial << "Using the NXP IMU" << ENDL;
  // Initialize the sensors.
  if(!gyro.begin())
  {
    /* There was a problem detecting the gyro ... check your connections */
    Serial.println("Ooops, no gyro detected ... Check your wiring!");
    while(1);
  }
   if(!accelmag.begin(ACCEL_RANGE_4G))
  {
    Serial.println("Ooops, no FXOS8700 detected ... Check your wiring!");
    while(1);
  }
#else
  Serial << "Using the LSM IMU " << ENDL;
  if (!lsm.begin())
  {
    /* There was a problem detecting the L3GD20 ... check your connections */
    Serial.println("Ooops, no LSM9DS0 detected ... Check your wiring!");
    while (1);
  }
#endif
  configureSensor();

#ifdef USE_MADGWICK
  Serial << "Using Madgwick filter ";
#else
  Serial << "Using Mahony filter ";
#endif

#ifdef USE_MAGNETOMETERS
  Serial << " (using magnetometers+accelerometers+gyros)." << ENDL;
#else
  Serial << " (NOT using magnetometers (accelerometers+gyros only)." << ENDL;
#endif

  if (ForceGyrosToZero) {
    Serial << "Gyros forced to 0 (DEBUGGING)" << ENDL; 
  }
#ifdef CALIBRATE_GYROS
  Serial << "Gyros are calibrated (0-rates)" << ENDL;
#endif
  
  // Filter expects a predefined number of samples per second
  const float sampleFrequency=1000.0f / AcquisitionPeriod;
  Serial << "Initializing filter with frequency (Hz) " << sampleFrequency << " (period=" << AcquisitionPeriod << ")" << ENDL;
  Serial << "Displaying every " << OutputPeriod << " msec" << ENDL;
  filter.begin(sampleFrequency);
  elapsed = elapsedDisplay = 0;
  previousTimestamp=millis();
}

void loop(void)
{
  if (elapsed >= AcquisitionPeriod) {
    timestamp = millis();
    if ((timestamp-previousTimestamp) != AcquisitionPeriod) {
        Serial << "Error: actual period is " << timestamp-previousTimestamp << " msec instead of " << AcquisitionPeriod << ENDL;
    }
    previousTimestamp=timestamp;
    
    elapsed = 0;
    float rmx,rmy,rmz,mx,my,mz, gx,gy,gz;
    sensors_event_t gyro_event;
    sensors_event_t accel_event;
    sensors_event_t mag_event;
#ifdef USE_NXP_IMU
  gyro.getEvent(&gyro_event);
  accelmag.getEvent(&accel_event, &mag_event);
  // Apply mag offset compensation to µTesla values (base values in uTesla)
  rmx = mag_event.magnetic.x - mag_offsets[0];
  rmy = mag_event.magnetic.y - mag_offsets[1];
  rmz = mag_event.magnetic.z - mag_offsets[2];
  
  /* OPEN ISSUE: I EXPECTED TO APPLY CALIBRATION TO RAW DATA AS FOLLOWS, SINCE CALIBRATION WAS PERFORMED
   * USING RAW DATA: */
#ifdef UNUSED
  rmx = accelmag.mag_raw.x - mag_offsets[0];
  rmy = accelmag.mag_raw.y - mag_offsets[1];
  rmz = accelmag.mag_raw.z - mag_offsets[2];
  rmx*= 0.1F; // This is the constant MAG_UT_LSM (0.1 µT/lsb)
  rmy*= 0.1F;
  rmz*= 0.1F;
 
 /* BUT THIS DOES NOT WORK, WHILE APPLYING TO µT VALUES WORK. WHY IS THAT ??? (see test log for hypothesis) */ 
  Serial << "Raw mag          : " << accelmag.mag_raw.x << " / " << accelmag.mag_raw.y 
         << " / " << accelmag.mag_raw.z 
         << "  Magn:" 
         << sqrt(accelmag.mag_raw.x*accelmag.mag_raw.x +accelmag.mag_raw.y*accelmag.mag_raw.y+accelmag.mag_raw.z*accelmag.mag_raw.z) 
         << ENDL;
#endif
   
  // Apply mag soft iron error compensation
   mx = rmx * mag_softiron_matrix[0][0] + rmy * mag_softiron_matrix[0][1] + rmz * mag_softiron_matrix[0][2];
   my = rmx * mag_softiron_matrix[1][0] + rmy * mag_softiron_matrix[1][1] + rmz * mag_softiron_matrix[1][2];
   mz = rmx * mag_softiron_matrix[2][0] + rmy * mag_softiron_matrix[2][1] + rmz * mag_softiron_matrix[2][2];
  
   gx = gyro_event.gyro.x ;
   gy = gyro_event.gyro.y ;
   gz = gyro_event.gyro.z ;

   // Apply gyro zero-rate error compensation
#  ifdef CALIBRATE_GYROS
   gx -= GyroZeroRate[0];
   gy -= GyroZeroRate[1];
   gz -= GyroZeroRate[2];
#  endif 

  // The filter library expects gyro data in degrees/s, but adafruit sensor
  // uses rad/s so we need to convert them first (or adapt the filter lib
  // where they are being converted)
  gx *= 57.2958F;
  gy *= 57.2958F;
  gz *= 57.2958F;
#else
    // Get new data samples
    lsm.getEvent(&accel_event, &mag_event, &gyro_event, NULL);

    // We calibrated the data in µT, since the parameters are obtained with MotionCal 
    // (see on-board SW document for details) 
    // Convert in µT: 1G = 100 µT hence 1 mG = 0.1 µT.
    constexpr float MicroT_PerLSB=LSM9DS0_MAG_MGAUSS_2GAUSS/10.0f;
#  ifdef PRINT_CALIBRATED_DATA
    rmx = lsm.magData.x*MicroT_PerLSB;
    rmy = lsm.magData.y*MicroT_PerLSB;
    rmz = lsm.magData.z*MicroT_PerLSB;
    Serial << "Mag in µT         : " << rmx << " / " << rmy << " / " << rmz
         << "  Magn:"  << sqrt(rmx*rmx+rmy*rmy+rmz*rmz)  << ENDL;
 #  endif
    rmx = lsm.magData.x*MicroT_PerLSB - mag_offsets[0];
    rmy = lsm.magData.y*MicroT_PerLSB - mag_offsets[1];
    rmz = lsm.magData.z*MicroT_PerLSB - mag_offsets[2];
 
    // Apply mag soft iron error compensation
    mx = rmx * mag_softiron_matrix[0][0] + rmy * mag_softiron_matrix[0][1] + rmz * mag_softiron_matrix[0][2];
    my = rmx * mag_softiron_matrix[1][0] + rmy * mag_softiron_matrix[1][1] + rmz * mag_softiron_matrix[1][2];
    mz = rmx * mag_softiron_matrix[2][0] + rmy * mag_softiron_matrix[2][1] + rmz * mag_softiron_matrix[2][2];

    //Convert Z value to restore right-handedness.
    mz = -mz;
    
#  ifdef CALIBRATE_GYROS
    gx  = (lsm.gyroData.x - GyroZeroRate[0]) * LSM9DS0_GYRO_DPS_DIGIT_245DPS;
    gy  = (lsm.gyroData.y - GyroZeroRate[1]) * LSM9DS0_GYRO_DPS_DIGIT_245DPS;
    gz  = (lsm.gyroData.z - GyroZeroRate[2]) * LSM9DS0_GYRO_DPS_DIGIT_245DPS;
#  else
    // The filter library expects gyro data in degrees/s, but adafruit sensor
    // uses rad/s so we need to convert them first (or adapt the filter lib
    // where they are being converted)
    // BDH: SEEMS TO ME THAT FOR LSM at least, gyros are returned in dps.
    //      REMOVED CONVERSION.
     gx = gyro_event.gyro.x; // * 57.2958F;
     gy = gyro_event.gyro.y; // * 57.2958F;
     gz = gyro_event.gyro.z; // * 57.2958F;
#  endif
#endif

    if (ForceGyrosToZero) {
      gx=gy=gz=0.0f;
    }
    
 #ifdef PRINT_CALIBRATED_DATA
    Serial << "Raw mag w/ offset: " << rmx << " / " << rmy << " / " << rmz << ENDL;
    Serial << "Cal mag          : " << mx << " / " << my << " / " << mz << "  Magn="
           <<  sqrt(mx*mx + my*my+ mz*mz) << ENDL;
    Serial << "Cal/raw mag      : " << mx/rmx << " / " << my/rmy << " / " << mz/rmz << ENDL;
    Serial << "Gyros (dps)      : " << gx << " / " << gy << " / " << gz << ENDL;
    Serial << "----" << ENDL;
#endif 

    // Update the filter
#ifdef USE_MAGNETOMETERS
    filter.update(gx, gy, gz,
                  accel_event.acceleration.x, accel_event.acceleration.y, accel_event.acceleration.z,
                  mx, my, mz);
#else
    filter.updateIMU(gx, gy, gz,
                     accel_event.acceleration.x, accel_event.acceleration.y, accel_event.acceleration.z);
#endif

#ifdef HUMAN_READABLE_OUTPUT
    if (elapsedDisplay >= OutputPeriod) {
      elapsedDisplay = 0;
      // Print the orientation filter output
      float roll = filter.getRoll();
      float pitch = filter.getPitch();
      float heading = filter.getYaw();
      Serial.print(millis());
      Serial.print(" - Orientation (R-Y-P, degrees) : ");
      Serial.print(roll);
      Serial.print(" ");
      Serial.print(heading);
      Serial.print(" ");
      Serial.println(pitch);
    }
#else
    if (elapsedDisplay >= OutputPeriod) {
      elapsedDisplay = 0;

      Serial << 100 << "," << timestamp << ","
             << accel_event.acceleration.x << ',' << accel_event.acceleration.y << ',' << accel_event.acceleration.z << ',';
#  ifdef USE_NXP_IMU
      // This is the gyro in rps
      Serial.print( gyro_event.gyro.x,6); Serial <<',';
      Serial.print( gyro_event.gyro.y,6); Serial <<',';
      Serial.print( gyro_event.gyro.z,6); Serial <<',';
      Serial << accelmag.mag_raw.x << ',' << accelmag.mag_raw.y << ',' << accelmag.mag_raw.z;
#  else      
      // This is the raw gyro data
      Serial << lsm.gyroData.x << "," << lsm.gyroData.y << "," << lsm.gyroData.z << ',';  
      // For LSM mag event values are in Gauss. 1G = 100 µT. 
      Serial << mag_event.magnetic.x*100.0f << ',' << mag_event.magnetic.y*100.0f << ',' << mag_event.magnetic.z*100.0f;
#  endif
      Serial << ",0,0,0,0," ; // GPS
      Serial << "0,0,0,0," ; // Primary
      Serial << "0," ; //Secundary
      // Ground record part
      Serial << accel_event.acceleration.x << ',' << accel_event.acceleration.y << ',' << accel_event.acceleration.z << ","
             << gx << "," << gy << "," << gz << ","
             << mx << ',' << my << ',' << mz;
      Serial << ",0,0,0," ; // AHRS Accel
      Serial << filter.getRollRadians() << "," << filter.getYawRadians() << "," << filter.getPitchRadians();
      for (auto i = 0; i < 24 ; i++) {
        Serial << ",0";
      }
      Serial.println();
    }
#endif
  }
}
