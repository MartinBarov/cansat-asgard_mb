/*
 * IMU_Client.h
 */
 
#pragma once

#include "IsaTwoRecord.h"

/** A abstract class to query an Inertial Management Unit and
 *  store the results into an IsaTwoRecord for broadcasting.
 */
class IMU_Client
{
  public:
	IMU_Client(byte nSamplesPerRead=1): numSamplesPerRead(nSamplesPerRead) {};
    /** Initialize driver. Call before using readData()
     *  @return true if initialization ok.
     *  @pre Serial and Wire must be initialized
     */
    virtual bool begin()=0;
    /** Make a reading reading of all 9 sensors, and store magnetic field in µT, gyro
     *  in raw sensor steps and rps, acceleration in raw sensor steps and m/s^2 to
     *  the current values in the record. The values are averaged on numSamplesPerRead samples
     *  @remark: this method seems to be useless: averaging values degrades the fusion
     *           result. Preferably use the readOneData() method.
     *  NB: the record timestamp is NOT modified.
     *  @param record The record to complete with the data.
     */
    virtual void readData(IsaTwoRecord& record);
  protected:
    /** Make an averaged reading of all 9 sensors, and store magnetic field in µT, gyro
     *  in raw sensor steps and rps, acceleration in raw sensor steps and m/s^2 to
     *  the current values in the record.
     *  NB: the record timestamp is NOT modified. This function is called when numSamples>1 only.
     *  @param record The record to update with the data.
     */
    virtual void readAverage(IsaTwoRecord& record);

    /** Make a single reading of all 9 sensors, and store magnetic field in µT, gyro
     *  in raw sensor steps and rps, acceleration in raw sensor steps and m/s^2 to
     *  the current values in the record.
     *  NB: the record timestamp is NOT modified.
     *  @param record The record to update with the data.
     */
    virtual void readOneData(IsaTwoRecord& record)=0;
  private:
    byte numSamplesPerRead;
};
