/*
   test_TC_Timer
   Unit test for class TC_Timer (SAMD21 boards only).
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "TC_Timer.h"
#define DEBUG_CSPU
#include "DebugCSPU.h"

#ifndef ARDUINO_ARCH_SAMD
#error "TC_Timer class only supports SAMD21 architecture"
#endif
#undef TEST_TC4	   // Define to test timer TC4. This is not possible if library Servo
				   // is installed, since it already defines a Handler for this timer.
constexpr uint16_t TC_timerFreq[3] = {1000, 2000,3000};    // Timer frequency in Hz, for TC3,  TC4, TC5 
constexpr uint32_t reportingPeriod = 3000; // Report frequency each reportingPeriod msec.
constexpr byte NumChecksPerTest = 3;     // Number of checks between two successive counter resets.
constexpr bool runTC3 = true;
#ifdef TEST_TC4
constexpr bool runTC4 = true;
#endif
constexpr bool runTC5 = true;

constexpr byte  LED_Pin=LED_BUILTIN; //just to blink the on-board LED

uint32_t counterTC[3] = {0, 0, 0}; // count the number of interrupts to check frequency 0= TC3, 1 = TC4....
uint32_t startTime = 0; // the clock value when the counter is reset.
TC_Timer tc3(TC_Timer::HwTimer::timerTC3);
#ifdef TEST_TC4
TC_Timer tc4(TC_Timer::HwTimer::timerTC4);
#endif
TC_Timer tc5(TC_Timer::HwTimer::timerTC5);

void setup() {
  pinMode(LED_Pin, OUTPUT); //this configures the LED pin, you can remove this it's just example code
  Serial.begin(115200);
  while (!Serial) ;
  Serial.println("Configuring timer(s)...");
  Serial.flush();
  if (runTC3) {
    Serial.print("Configuring TC3...");
    Serial.flush();
    tc3.configure(TC_timerFreq[0]); //configure the timer to run at 1000/timerPeriod Hertz
    tc3.enable(); //starts the timer
    Serial.println("Done.");
  }
#ifdef TEST_TC4
  if (runTC4) {
    Serial.println("Configuring TC4...");
    tc4.configure(TC_timerFreq[1]); //configure the timer to run at 1000/timerPeriod Hertz
    tc4.enable(); //starts the timer
  }
#else
  Serial.println("Skipping TC4...");
#endif

  if (runTC5) {
    Serial.println("Configuring TC5...");
    tc5.configure(TC_timerFreq[2]); //configure the timer to run at 1000/timerPeriod Hertz
    tc5.enable(); //starts the timer
  }
  Serial.println("Setup OK");
}



void checkTimerPeriod() {
  auto now = millis();
  auto duration = now - startTime;
  Serial << "After " << duration / 1000 << " s: ";
  for (int i = 0 ; i < 3 ; i++) {
#ifndef TEST_TC4
	if (i+3 == 4) continue;
#endif
    float freqTC = ((float) counterTC[i] / duration * 1000);
    Serial << "| TC" << i+3 << ": count="  << counterTC[i] << ", freq=" << freqTC << " Hz (expected " << TC_timerFreq[i] 
           << ", err= " << 100.0*(freqTC-TC_timerFreq[i])/TC_timerFreq[i] << "%)";
  }
  Serial << ENDL;
}

void loop() {
  static uint16_t i = 0;
  if ((i % (NumChecksPerTest + 1)) == 0) {
    // start new test
    startTime = millis();
    counterTC[0] = counterTC[1] = counterTC[2] = 0;
    Serial.println("------");
  }
  else checkTimerPeriod();
  i++;
  delay(reportingPeriod);
  //tc.disable(); //This function can be used anywhere if you need to stop/pause the timer
  //tc.reset(); //This function should be called everytime you stop the timer
}

//this function gets called by the interrupt
void TC5_Handler (void) {
  if (tc5.isYourInterrupt()) {
    counterTC[2]++;
    digitalWrite(LED_Pin, !digitalRead(LED_Pin));
    tc5.clearInterrupt(); // clear the interrupt so that it will run again
  }
}

#ifdef TEST_TC4
void TC4_Handler (void) {
  if (tc4.isYourInterrupt()) {
    counterTC[1]++;
    tc4.clearInterrupt(); // clear the interrupt so that it will run again
  }
}
#endif

void TC3_Handler (void) {
  if (tc3.isYourInterrupt()) {
    counterTC[0]++;
    tc3.clearInterrupt(); // clear the interrupt so that it will run again
  }
}
