/*
   Test for classes SD_Logger & FlashLogger.

   Tested here for the SD_Logger:
      Initializing SD_Logger on absent pin
      No init
      Test various init conditions
      Do multiple inits
      Logging
      Test perfotmance
   
   Other tests:
      cansatAsgardCSPU/test_SD_Logger2
      SpySeeCanCSPU/test_SD_Logger


   if TEST_FLASH_LOGGER is not defined, this program tests the SD_Logger:
      It supposes the SD_Reader is available.
      Test 1 should succeed when no card is inserted, others should succeed
       when a card is inserted.


      Still not tested: card full condition
   if TEST_FLASH_LOGGER is defined, this program tests the FlashLogger 
        (using the Feather M0 _Express on-board flash.
      It obviously assumes a Feather M0 Express board.
      Test 1 should always succeed.
      Still not tested: flash full condition.

   Wiring: 
      Uno: CS to D4, DI (MOSI) = D11, DO (MISO) = D12, SCK = 13, GND to GND, 5V to 5V
      Feather M0 Express: CS to 10, DI to MO (MOSI), DO to MI (MISO), CLK to SCK, GND to GND, 3V to 3V.
      Feather M0 Adalogger: no wiring required, CS is 4 (internal pin). 
   
   Known issues:
      At least for the FlashLogger, just using 2 instances causes problem (even if the second one is
      created after the first one went out-of-scope, and even though available memory is not an issue).
      Defining symbol TEST_SEVERAL_SIMULTANEOUS_INSTANCES causes test 2b to create a second instance,
      and it causes the next call to fatfs::begin() to block in the test 3. Exact cause is not known.
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

//#define TEST_FLASH_LOGGER   // Define to test FlashLogger, rather than SD_Logger

const byte CS_Pin = 4; // change to 10 if using Feather M0 express

constexpr byte UnusedPin= 6; // Do not use any of the number used on Uno (10-11-12-13) or Feather .
//#define FILL_STORAGE


#include "elapsedMillis.h"
#ifdef TEST_FLASH_LOGGER
#  ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#    error Cannot test FlashLogger on this board (Feather M0 Express expected)
#  endif
#  include "FlashLogger.h"
   constexpr unsigned int RequestedFreeMegs = 1;
   // For the FlashLogger, only one instance is supported (cf. header);
   FlashLogger    theLogger;
#else
#  include "SD_Logger.h"
   SD_Logger theLogger(CS_Pin);
   constexpr unsigned int RequestedFreeMegs = 100;
#endif

#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"

bool cardOK = false;

unsigned long fileSize(const String& fileName);

void test0_NoModule() {
#ifdef TEST_FLASH_LOGGER
  Serial << F("Test 0 skipped (not relevant for FlashLogger)") << ENDL;
#else
  Serial << F("----------------") << ENDL;
  Serial << F("0. Testing absent SD module on pin ") << UnusedPin << ENDL;
  digitalWrite(CS_Pin, HIGH);
  pinMode(UnusedPin, OUTPUT); // set the right Pin to HIGH to make sure the SD card will NOT answer. 
  SD_Logger SD_absent( UnusedPin);
  
  auto result = SD_absent.init("abcd");
  // Should fail (wrong CS_Pin)
  Serial << F("Result=") << (int) result ;
  DASSERT(result < 0);
  Serial <<  F(": OK") << ENDL;
#endif
}

void test1_NoInit() {
#ifdef TEST_FLASH_LOGGER
  Serial << F("Test 1 skipped (not relevant for FlashLogger)") << ENDL;
  cardOK = true;
#else
  Serial << F("Testing with an SD Module...") << ENDL;
  auto result = theLogger.init("abcd", "NoInit");
  
  if (result < 0) {
    Serial << F("results=") << (int) result << F(". Test with no card OK. Others skipped") << ENDL;
    cardOK = false;
  }
  else if (result == 1) {
    Serial << F("Not enough space on card.") << ENDL;
    cardOK = false;
  }
  else {
    Serial << F("results=") << (int) result << F(". Test with no card skipped. Performing other tests") << ENDL;
    cardOK = true;
  }
#endif
}

void test2b_multipleInit()
{

  {
    Serial << F("----------------") << ENDL;
    Serial << F("2b Multiple inits") << ENDL;
    auto result = theLogger.init("abcd");
    DASSERT(result == 0);
    Serial << F(" first init OK. File is ") << theLogger.fileName() << ENDL;
    Serial << F("Initializing same object again (should be noop, use same file and succeed)") << ENDL;
    result = theLogger.init("abcd","firstLine");
    if (result) {
      Serial << F("  Second init failed with result=") << result << ENDL;
    } else {
      Serial << F("   Second init OK. Current file is now ") << theLogger.fileName() << ENDL;

    }   
    Serial << F("Initializing same object again (should just open a new file)") << ENDL;
    result = theLogger.init("abcd");
    if (result) {
      Serial << F("  Second init failed with result=") << result << ENDL;
    } else {
      Serial << F("   Second init OK. Current file is now ") << theLogger.fileName() << ENDL;

    }
  }

#ifdef TEST_SEVERAL_SIMULTANEOUS_INSTANCES
#ifndef TEST_FLASH_LOGGER
  SD_Logger theLogger3(CS_Pin);
#endif
  Serial << F("  -Initializing new object after previous ones are out-of-scope (should succeed)") << ENDL;
  auto result = theLogger3.init("abcd");
  DASSERT(result == 0);
  Serial << F(" new init OK") << ENDL;
#endif
}


void test2_VariousInitConditions()
{
  Serial << "----------------" << ENDL;
  Serial << F("2. Init conditions") << ENDL;
  Serial << F("Requesting more space than available") << ENDL;
  auto result = theLogger.init("abcd", "", 32000 );
  // Should fail (not enough space)
  DASSERT(result == 1);
  Serial << F("OK") << ENDL;

  // Test init with empty message
  Serial << F("Init with empty msg...");
  elapsedMillis elapsed=0;
  result = theLogger.init("abcd", "");
  Serial << elapsed << F(" msec") << ENDL;
  DASSERT(result == 0);
  DASSERT(theLogger.fileSize() == 0);

  // Check structure of fileName()
  String name = theLogger.fileName();
  DASSERT(name.startsWith("abcd"));
  DASSERT(name.endsWith(".txt"));
  String numStr = name.substring(4, 8);
  DASSERT(numStr.charAt(0) == '0');
  int num = numStr.toInt();
  DASSERT(num > 0);
  DASSERT(num < 1000);
  Serial << F("OK") << ENDL;

  // Test init with non empty message.
  String str = "012345678901234567890123456789012345";
  Serial << F("Init with non - empty msg... ");
  elapsed=0;
  result = theLogger.init(str, "abcd");
    Serial << elapsed << F(" msec") << ENDL;
  DASSERT(result == 0);
  Serial << F("Checking length of file '") << theLogger.fileName() << "'" << ENDL;
  Serial << F("str.length() = ") << str.length() << F(", file size = ") << theLogger.fileSize() << ENDL;
  DASSERT(theLogger.fileSize() == (str.length() + 2)); // End of line is 2 characters.
  Serial << F("OK") << ENDL;
}

void test3_logging()
{
  Serial << "----------------" << ENDL;
  Serial << F("3. Logging to file... ");
  auto result = theLogger.init("abcd", "", RequestedFreeMegs);
  DASSERT(result == 0);
  unsigned long size = theLogger.fileSize();
  String str = F("first line");
  size += (str.length() + 2);
  elapsedMillis elapsed=0;
  bool bresult = theLogger.log(str);
  Serial << elapsed << F(" msec ");

  DASSERT(bresult);
  DASSERT(theLogger.fileSize() == size);
  str = F("second line");
  size += (str.length() + 2);
  elapsed=0;
  bresult = theLogger.log(str);
  Serial << elapsed << F(" msec ");
  DASSERT(bresult);
  DASSERT(theLogger.fileSize() == size);

  str = F("third line");
  size += (str.length() + 2);
  elapsed=0;
  bresult = theLogger.log(str);
  Serial << elapsed << F(" msec") << ENDL;
  DASSERT(bresult);
  DASSERT(theLogger.fileSize() == size);


  str = F("fourth line, is much longer than the previous ones. This is to test how large strings are managed by the library");
  str += F(" and check for possible overflow. Since we will not log more than about 50 characters at a time, this should be ");
  str += F(" more than we need. \n(There is a \\n character just before this parenthesis).");
  size += (str.length() + 2);
  bresult = theLogger.log(str);
  DASSERT(bresult);
  DASSERT(theLogger.fileSize() == size);

  Serial << F("OK") << ENDL;
}

void test4_performance() {
  const byte numWrites = 50;
  bool bresult;
  String refStr = F("0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
 
  Serial << "----------------" << ENDL;
  Serial << F("4. Checking performance : writing ") << numWrites << F(" x ") << refStr.length() << F(" bytes to file") << ENDL;
  auto result = theLogger.init("abcd", "", RequestedFreeMegs );
  DASSERT(result == 0);
  elapsedMillis fromStart = 0;
  for (int i = 0 ; i < numWrites; i++) {
    bresult = theLogger.log(refStr);
    DASSERT(bresult);
  }
  long duration = fromStart;
  float oneWriteDuration = ((float) duration) / numWrites;
  Serial << F("Elapsed time = ") << duration << F(" msec.") << ENDL;
  Serial << F("This is an average ") << oneWriteDuration << F(" msec / 100 bytes written") << ENDL;

  Serial << ENDL << F("triggering maintenance (. = 500 ms), should happen every 10 sec.") << ENDL;

  while (!theLogger.doIdle()) {
    delay(500);
    Serial << ".";
    Serial.flush();
  }

  Serial << F("Writing again... ") << numWrites << F(" strings of increasing length") << ENDL;
  String str = F("");
  fromStart = 0;
  char buf[8];
  for (int i = 0 ; i < numWrites; i++) {
    str += itoa(i, buf, 10);
    bresult = theLogger.log(str);
    DASSERT(bresult);
  }
  duration = fromStart;
  oneWriteDuration = ((float) duration) / numWrites;
  Serial << F("Elapsed time = ") << duration << F(" msec.") << ENDL;
  Serial << F("This is an average ") << oneWriteDuration << F(" msec / 100 bytes written") << ENDL;
  Serial << F("*** Check content of file '") << theLogger.fileName() << F("'. It should contain all ") << numWrites-1 << F(" first integers in the last line.") << ENDL;

  Serial << ENDL << F("Switching to next file") << ENDL;
  result = theLogger.init("abcd");
  DASSERT(result == 0);
  Serial << ENDL << F("triggering maintenance (. = 500 ms), should happen every 10 sec.") << ENDL;

  while (!theLogger.doIdle()) {
    delay(500);
    Serial << ".";
    Serial.flush();
  }

  Serial << F("Writing all data in 1 call call to log...(");
  String longStr = "";
  longStr.reserve(numWrites * 100);
  for (int i = 0 ; i < numWrites; i++) {
    longStr += refStr;
  }
  Serial << longStr.length() << F(" bytes)") << ENDL;
  Serial.flush();
  fromStart = 0;
  bresult = theLogger.log(longStr);
  DASSERT(bresult);
  duration = fromStart;
  oneWriteDuration = duration / numWrites;
  Serial << F("Elapsed time = ") << duration << F(" msec.") << ENDL;
  Serial << F("This is an average ") << oneWriteDuration << F(" msec / 100 bytes written ") <<  ENDL;
}

#ifdef FILL_STORAGE
void test_fillingUpTheCard()
{
  const char* bigFileName = "bigfile.txt";

  // Copy file until free space < 1000 bytes.
  DASSERT(theLogger.begin(chipSelectPinNumber));
  if (!theLogger.exists(bigFileName)) {
    Serial << F("Error : please provide 1Mb file named ") << bigFileName << ENDL;
  }
  File dataFile = theLogger.open(bigFileName, FILE_READ);
  DASSERT(dataFile);
  unsigned long fileSize = dataFile.fileSize();
  dataFile.close();
  auto result = logger.init("abcd", CS_Pin, "", RequestedFreeMegs );
  DASSERT(result == 0);
  while (logger.freeSpaceInBytes() > fileSize) {

  }

  // Log more than 1000 bytes.
  Serial << F("Checking how card full is handled...") << ENDL;


  // Create a 2Megs file
  unsigned long
}
#endif

void printSomeCppSymbols()
{
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS 
  Serial << F("ARDUINO_SAMD_FEATHER_M0_EXPRESS is defined") << ENDL;
#endif
#ifndef TEST_FLASH_LOGGER

  // SdFat library symbols. 
  Serial << F("USE_STANDARD_SPI_LIBRARY=") << USE_STANDARD_SPI_LIBRARY << ENDL;
  Serial << F("SD_HAS_CUSTOM_SPI=") << SD_HAS_CUSTOM_SPI << ENDL;
#endif
}
//-----------------------------------------------------------------------------------------------------------------------

void setup() {
  DINIT(115200);
  printSomeCppSymbols();
#ifdef TEST_FLASH_LOGGER
  Serial << F("Testing FlashLogger...") << ENDL;
#else
  Serial << F("Testing SD_Logger...") << ENDL;
  pinMode(CS_Pin, OUTPUT);
  SPI.begin();
#endif

#ifdef LOGGER_CLOSE_AFTER_EACH_ACCESS
  Serial << F("NB : Logger closes the file after each access.") << ENDL;
#else
  Serial << F("NB : Logger does NOT close the file after each access.") << ENDL;
#endif
  test0_NoModule();
  test1_NoInit();
  if (cardOK) {
    //Run other tests.
#ifdef FILL_STORAGE
    test_fillingUpTheCard();
#else
    test2_VariousInitConditions();
    test2b_multipleInit();
    test3_logging();
    test4_performance();
#endif
  }
  Serial << F("End of tests.") << ENDL;

}

void loop() {
  delay(100);

}
