/*
    TC_Timer.cpp

    Info:
    symbol GCM_TC4_TC5 is defined in WVariant.h, which, on MacOS is located in
    /Library/Arduino15/packages/adafruit/hardware/samd/1.2.8/cores/arduino

    For an Adafruit Feather M0 Express bord, Symbol TC5 is defined in file samd21e18a.h
    which, on macOS, is located at
    ~/Library/Arduino15/packages/arduino/tools/CMSIS-Atmel/1.2.0/CMSIS/Device/ATMEL/samd21/include
    Its type is (Tc *).

*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "TC_Timer.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_DIAGNOSTIC 1
#define DBG_CONFIGURE 0

#ifdef ARDUINO_ARCH_SAMD
/* This code currently only applies to SAMD21 processors
   No error is triggered when compiled for another board, in order to allow using
   the other elements of the library on other board. Obviously, trying to instanciate
   the class on non SAMD board will result in undefined symbols at link time.
*/
#  ifndef __SAMD51__  // This is only for SAMD21

#define CPU_HZ 48000000

TC_Timer::TC_Timer(HwTimer hwTimer) {
  switch (hwTimer) {
    case HwTimer::timerTC3:
      tcTimer = TC3;
      break;
    case HwTimer::timerTC4:
      tcTimer = TC4;
      break;
    case HwTimer::timerTC5:
      tcTimer = TC5;
      break;
    default:
      tcTimer = NULL;
  };
}

void TC_Timer::setFrequency(uint16_t frequencyHz) {
  // Warning: 1024 is the timer prescaler and must be consistent with the configuration of the timer.
  uint16_t compareValue = floor((CPU_HZ / (1024.0 * frequencyHz))  - 1.0 + 0.5);
  // Make sure the count is in a proportional position to where it was
  // to prevent any jitter or disconnect when changing the compare value.
  tcTimer->COUNT16.COUNT.reg = map(tcTimer->COUNT16.COUNT.reg, 0, tcTimer->COUNT16.CC[0].reg, 0, compareValue);
  tcTimer->COUNT16.CC[0].reg = compareValue;
  DPRINTS(DBG_CONFIGURE, "freq=");
  DPRINTLN(DBG_CONFIGURE, frequencyHz);
  DPRINTS(DBG_CONFIGURE, "counter value=");
  DPRINTLN(DBG_CONFIGURE, tcTimer->COUNT16.COUNT.reg);
  DPRINTS(DBG_CONFIGURE, "Compare value=COUNT16.CC[0]=");
  DPRINTLN(DBG_CONFIGURE, tcTimer->COUNT16.CC[0].reg);
  while (isSyncing());
}

void TC_Timer::configure(uint16_t frequencyHz)
{
  // Use the CPU clock as source.
  if (tcTimer == TC3) {
    REG_GCLK_CLKCTRL = (uint16_t) (GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID_TCC2_TC3) ;
    DPRINTS(DBG_CONFIGURE, "GCLK for TC3: REG_GCLK_CLKCTL=");
  } else {
    REG_GCLK_CLKCTRL = (uint16_t) (GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID(GCM_TC4_TC5)) ;
    DPRINTS(DBG_CONFIGURE, "GCLK for TC4-5: REG_GCLK_CLKCTL=");
  }
  DPRINTLN(DBG_CONFIGURE, REG_GCLK_CLKCTRL);
  while ( GCLK->STATUS.bit.SYNCBUSY == 1 ); // wait for sync

  tcTimer->COUNT16.CTRLA.reg &= ~TC_CTRLA_ENABLE;
  while (isSyncing()); // wait for sync

  // Use the 16-bit timer
  tcTimer->COUNT16.CTRLA.reg |= TC_CTRLA_MODE_COUNT16;
  while (isSyncing()); // wait for sync

  // Use match mode so that the timer counter resets when the count matches the compare register
  tcTimer->COUNT16.CTRLA.reg |= TC_CTRLA_WAVEGEN_MFRQ;
  while (isSyncing()); // wait for sync

  // Set prescaler to 1024
  tcTimer->COUNT16.CTRLA.reg |= TC_CTRLA_PRESCALER_DIV1024;
  while (isSyncing()); // wait for sync

  setFrequency(frequencyHz);

  // Enable the compare interrupt
  tcTimer->COUNT16.INTENSET.reg = 0;
  tcTimer->COUNT16.INTENSET.bit.MC0 = 1;

  // Configure interrupt request
  IRQn_Type IRQn;
  if (tcTimer == TC3) {
    IRQn = TC3_IRQn;
    DPRINTSLN(DBG_CONFIGURE, "IRQ for TC3");
  }
  else if (tcTimer == TC4) {
    IRQn = TC4_IRQn;
    DPRINTSLN(DBG_CONFIGURE, "IRQ for TC4");
  }
  else if (tcTimer == TC5) {
    IRQn = TC5_IRQn;
    DPRINTSLN(DBG_CONFIGURE, "IRQ for TC5");
  }
  else {
    DPRINTSLN(DBG_DIAGNOSTIC, "Error: unexpected TC timer!");
    return; // Avoid configuring with uninitialized IRQn
  }

  NVIC_DisableIRQ(IRQn);
  NVIC_ClearPendingIRQ(IRQn);
  NVIC_SetPriority(IRQn, 0);
  NVIC_EnableIRQ(IRQn);  // CETTE INSTRUCTION BLOQUE LA CARTE AVEC TC3

  tcTimer->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;
  while (isSyncing()); // wait for sync

  // Debugging
  DPRINTS(DBG_CONFIGURE, "COUNT16.CTRLA.reg=");
  DPRINTLN(DBG_CONFIGURE, tcTimer->COUNT16.CTRLA.reg);
  DPRINTS(DBG_CONFIGURE, "COUNT16.INTENSET.reg=");
  DPRINTLN(DBG_CONFIGURE, tcTimer->COUNT16.INTENSET.reg);
}

bool TC_Timer::isSyncing()
{
  return tcTimer->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY;
}

void TC_Timer::enable()
{
  tcTimer->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE; //set the CTRLA register
  while (isSyncing()); //wait until snyc'd
}

void TC_Timer::reset()
{
  tcTimer->COUNT16.CTRLA.reg = TC_CTRLA_SWRST;
  while (isSyncing());
  while (tcTimer->COUNT16.CTRLA.bit.SWRST);
}

void TC_Timer::disable()
{
  tcTimer->COUNT16.CTRLA.reg &= ~TC_CTRLA_ENABLE;
  while (isSyncing());
}

void TC_Timer::clearInterrupt() {
  tcTimer->COUNT16.INTFLAG.bit.MC0 = 1; //Writing a 1 to INTFLAG.bit.MC0 clears the interrupt so that it will run again
}

bool TC_Timer::isYourInterrupt() {
  return (tcTimer->COUNT16.INTFLAG.bit.MC0 == 1);
}

#  else
#  warning "TC_Timer class not supported on SAMD51 boards."
#  endif // known board
#endif // ARCH_SAMD
