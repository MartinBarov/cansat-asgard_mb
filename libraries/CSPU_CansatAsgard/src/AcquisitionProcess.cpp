/*
   AcquisitionProcess.cpp

   Created LED_State::On: 19 janv. 2018
*/

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#define USE_ASSERTION
#undef USE_TIMER
#include "DebugCSPU.h"
#include "Timer.h"

#include "AcquisitionProcess.h"

#define DBG_INIT_ACQ_PROCESS 0
#define DBG_RUN 0
#define DBG_DIAGNOSTIC 1

AcquisitionProcess::AcquisitionProcess(
  unsigned int acquisitionPeriodInMsec) :  hwScanner(nullptr)
{
  periodInMsec = acquisitionPeriodInMsec;
  elapsedTime = acquisitionPeriodInMsec + 1; // Ensure next run will trigger an acquisition cycle.
  heartbeatLED_State = LED_State::Off;
}

AcquisitionProcess::~AcquisitionProcess() {
	if (hwScanner) {
		delete hwScanner;
	}
}

void AcquisitionProcess::blinkHeartbeatLED() {
  if ( (heartbeatTimer >= 1000) && (heartbeatLED_State == LED_State::Off) ) {
    setLED(LED_Type::Heartbeat, LED_State::On);
    heartbeatTimer = 0 ;
    heartbeatLED_State = LED_State::On;
  }

  if ((heartbeatTimer >= 1000) && (heartbeatLED_State == LED_State::On) ) {
    setLED(LED_Type::Heartbeat, LED_State::Off);
    heartbeatTimer = 0 ;
    heartbeatLED_State = LED_State::Off;
  }
}

void AcquisitionProcess::run() {
  blinkHeartbeatLED();

  if (elapsedTime >= periodInMsec) {
    DBG_TIMER("AcqProc::runReal");
    elapsedTime = 0;    // Reset timer here, to avoid adding processing time to the cycle duration.
    DPRINTSLN(DBG_RUN, "Running Process");
    setLED(LED_Type::Acquisition, LED_State::On);
    acquireDataRecord();
    setLED(LED_Type::Acquisition, LED_State::Off);

    if (isRecordRelevant()) {
      setLED(LED_Type::Storage, LED_State::On);
      storeDataRecord(measurementCampaignStarted());
      setLED(LED_Type::Storage, LED_State::Off);
    }
  } else doIdle();
}

void AcquisitionProcess::initLED_Hardware()  {
  HardwareScanner *hw = getHardwareScanner();
  DASSERT(hw != nullptr)
  byte pin;
  pin=hw->getLED_PinNbr(LED_Type::Init);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(LED_Type::Storage);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(LED_Type::Transmission);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(LED_Type::Acquisition);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(LED_Type::Heartbeat);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(LED_Type::Campaign);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(LED_Type::UsingEEPROM);
  if (pin) pinMode(pin, OUTPUT);

  setLED(LED_Type::Init, LED_State::On);
  setLED(LED_Type::Storage, LED_State::On);
  setLED(LED_Type::Transmission, LED_State::On);
  setLED(LED_Type::Acquisition, LED_State::On);
  setLED(LED_Type::Heartbeat, LED_State::On);
  setLED(LED_Type::Campaign, LED_State::On);
  setLED(LED_Type::UsingEEPROM, LED_State::On);
  delay(1000);  // All LEDs on for 1000 ms.
  setLED(LED_Type::Init, LED_State::Off);
  setLED(LED_Type::Storage, LED_State::Off);
  setLED(LED_Type::Transmission, LED_State::Off);
  setLED(LED_Type::Acquisition, LED_State::Off);
  setLED(LED_Type::Heartbeat, LED_State::Off);
  setLED(LED_Type::Campaign, LED_State::Off);
  setLED(LED_Type::UsingEEPROM, LED_State::Off);
  DPRINTSLN(DBG_INIT_ACQ_PROCESS,"initLED_Hardware done")
}

void  AcquisitionProcess::init() {
  DPRINTSLN(DBG_INIT_ACQ_PROCESS,"IN AcquisitionProcess::init")
  hwScanner=getNewHardwareScanner(); // Make sure to initialize the scanner before calling anything that will need it!
  if (!hwScanner) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "*** Fatal error: cannot allocate the HardwareScanner! ***")
  }
  hwScanner->init();
  initLED_Hardware();
  setLED(LED_Type::Init, LED_State::On);
  setLED(LED_Type::Campaign, LED_State::On); // This LED remains On Until campaign is started.
  initCansatProject();			// Common Cansat initialisation
  initSpecificProject();
  setLED(LED_Type::Init, LED_State::Off);
  DPRINTSLN(DBG_INIT_ACQ_PROCESS,"AcquisitionProcess::init done")

}

void AcquisitionProcess::setLED(LED_Type type, LED_State status) {
  byte pinNumber = getHardwareScanner()->getLED_PinNbr(type);
  if (pinNumber==0) return;
#ifdef ARDUINO_TMinus
  digitalWrite(pinNumber, (status==LED_State::On) ? LOW : HIGH);
  // write LOW sets LED ON on TMINUS.
#else
  digitalWrite(pinNumber, (status==LED_State::On) ? HIGH : LOW);
#endif
}

HardwareScanner* AcquisitionProcess::getNewHardwareScanner() {
	DPRINTSLN(DBG_INIT, "AcquisitionProcess::getNewHardwareScanner");
	return new (HardwareScanner);
}

