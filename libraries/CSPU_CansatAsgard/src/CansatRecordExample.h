/*
 * CansatRecordExample.h
 */

#pragma once
#include "CansatRecord.h"
#include "CansatConfig.h"
#include "CansatInterface.h"

/**  @ingroup CSPU_CansatAsgard
 *   @brief A subclass of CansatRecord, used as example and for tests.
 * 	 This class is not used operationally, but included in the library
 * 	 because it is shared by several tests, and particularly useful as
 * 	 an example for subclassing CansatRecord
 *
 * @warning: This class is used in the test of the CansatRecord, as well as in the
 *           test of the CansatXBeeClient class and in the half-duplex tranmission
 *           tests. Do no modify it without ensuring compatibility
 */
class CansatRecordExample: public CansatRecord {
public:
	uint16_t integerData; 	/**< Secondary mission pseudo data */
	float floatData;	    /**< Secondary mission pseudo data */

	// Test-only methods
	void initValues() ;
	bool checkValues(bool ignoreTimestamp=false);

	// Some constants shared by test programs.
	static constexpr const char* testString=
			"This is a reasonably long test string for the CansatXBeeClient. How nice!";
	static constexpr CansatFrameType TestStringType=CansatFrameType::StatusMsg;
	static constexpr uint8_t SequenceNumber=37;


protected:
	// Methods overridden by any subclass. See documentation in base class.
	virtual void printCSV_SecondaryMissionData(Stream & str,
			bool startWithSeparator, bool finalSeparator) const ;

	virtual void printCSV_SecondaryMissionHeader(Stream & str,
			bool startWithSeparator, bool finalSeparator) const ;

	virtual void clearSecondaryMissionData() ;

	virtual uint16_t getSecondaryMissionMaxCSV_Size() const ;

	virtual uint16_t getSecondaryMissionCSV_HeaderSize() const ;

	virtual void printSecondaryMissionData(Stream& str) const ;

    virtual uint8_t writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const ;

    virtual uint8_t readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize);

    virtual uint8_t getBinarySizeSecondaryMissionData() const;
};
