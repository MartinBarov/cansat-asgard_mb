#pragma once

#include "Arduino.h"
#include "CansatConfig.h"

#define DBG_CST_RECORD_TEMPLATE 0		// Set to 1 for debugging output in template functions.
#define DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE 1 // Set to 0 to remove diagnostic on error.

/** @ingroup CSPU_CansatAsgard
    @brief 	The record carrying the data acquired or computed by any CanSat:
    		primary mission data, GPS data, etc.  This class is intended to be subclassed
    		in each project to include and process the secondary mission data.
    @par Usage
    		1. Define or undefine the following symbols in CansatConfig.h:
    			INCLUDE_GPS_VELOCITY, INCLUDE_THERMISTOR2,
    		    INCLUDE_THERMISTOR3, INCLUDE_DESCENT_VELOCITY,
    		    INCLUDE_DESCENT_VELOCITY to include the corresponding data .
    		2. Create xxxxRecord class, inheriting from CansatRecord.
    		3. In xxxxRecord:
    			- Declare public class variables for the secondary mission data;
    			- Implement methods printCSV_SecondaryMissionHeader(),
    			  printCSV_SecondaryMissionData()
    			  clearSecondaryMissionData(), getSecondaryMissionMaxCSV_Size() and
    			  getSecondaryMissionMaxCSV_HeaderSize() (see their documentation bloc
    			  for details).
    			- Optionally implement methods print_SecondaryMissionData().

			@see CansatRecordExample as an example of implementation of a subclass.
*/
class CansatRecord  {
  public:

    CansatRecord();
    virtual ~CansatRecord() {};

    /** Enum values, allowing for restricting the CSV output to a subset of the record */
    enum class DataSelector {
      All,         	/**< Select complete record */
      GPS,    		/**< Select data from the GPS modules only */
      Primary, 		/**< Select data from primary mission only */
      Secondary,	/**< Select data from secondary mission only */
      PrimarySecondary, 	/**< Select data from primary and secondary sensors only */
      AllExceptSecondary 	/**< Select complete record except the secondary mission */
    } ;

    /** Enum values, used to select what is printed: Header or content */
    enum class HeaderOrContent {
      Header, /**< Select CSV header*/
      Content /**< Select CSV content*/
    } ;

    /** Stream the record in human-readable format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                   complete records, if the data must be parsed by code expecting a SSC_Record).
    */
    void print(Stream& str, DataSelector select = DataSelector::All) const;

    /** Stream the record/header in CSV format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                      complete records, if the data must be parsed by code expecting a SSC_Record).
        @param headerOrContent HeaderOrContent::Content if content should be printed, HeaderOrContent::Header if header should be printed.
        @warning  If the CSV output gets changed, SSC_Record::MaxCSV_Size and SSC_Record::MaxCSV_HeaderSize should imperatively be changed to 
                  (a little bit bigger than) the length of, respectively, the CSV content, and the CSV header.
    */
    void printCSV(Stream& str, DataSelector select = DataSelector::All, HeaderOrContent headerOrContent = HeaderOrContent::Content) const;

    /** Serialize the record in binary format in the destination buffer.
     *  If the bufferSize is insufficient, nothing is written. It should be at least getBinarySize() bytes.
     *  @param destinationBuffer The buffer to write into
     *  @param bufferSize The size of the provided destination buffer.
     *  @return True if everything ok, false otherwise. If true, you may assume the record
     *  was properly written and its binary size is getBinarySize();
    */
    bool writeBinary(uint8_t* const destinationBuffer, const uint8_t bufferSize) const;

    /** Read the record in binary format from the source buffer.
     *  If the bufferSize is insufficient, an error is reported.
     *  @param sourceBuffer The buffer to read from.
     *  @param bufferSize The size of the provided destination buffer.
     *  @return True if everything ok, false otherwise.
     */
    bool readBinary(const uint8_t* const sourceBuffer, const uint8_t bufferSize);

    /** Set all values to 0 or false */
    void clear();

    /** Obtain the maximum length of a record in CSV format */
    uint16_t getMaxCSV_Size() const;

    /** Obtain the maximum length of a record header in CSV format */
    uint16_t getCSV_HeaderSize() const;

    /** Obtain the size of the record in binary format (as written by the writeBinary() method */
    uint8_t getBinarySize() const;

    // Data members
    unsigned long timestamp{};  /**< Record timestamp in msec. */

    // A. GPS data
    bool  newGPS_Measures;        /**< true if GPS data is included in the record */
    double GPS_LatitudeDegrees;    /**< The latitude in decimal degrees, ([-90;90], + = N, - = S)
     	 	 	 	 	 	 	        A double is required because we need 10E-5 accuracy and a float
     	 	 	 	 	 	 	        only provides about 7 decimal digits of accuracy
     	 	 	 	 	 	 	        (-76.54321 and -76.54322 have the same representation)*/
    double GPS_LongitudeDegrees;   /**< The longitude in decimal degrees ([-180;180], + =E, - =W)
     	 	 	 	 	 	 	        A double is required because we need 10E-5 accuracy and a float
     	 	 	 	 	 	 	        only provides about 7 decimal digits of accuracy. */
    float GPS_Altitude;           /**< Altitude of antenna, in meters above mean sea level (geoid) */
#ifdef INCLUDE_GPS_VELOCITY
    float GPS_VelocityKnots;      	/**< velocity over ground in Knots (1 knot = 0.5144447 m/s) */
    float GPS_VelocityAngleDegrees; /**< Direction of velocity in decimal degrees, 0 = North */
#endif

    // B. Primary mission data
    float temperatureBMP;   	   	 /**< The temperature in °C obtained from the BMP. It could be
     	 	 	 	 	 	 	 	 	  BMP_Client::InvalidTemperature if sensor reading was impossible.
    									  Be sure to always test against this constant before using the value */
    float pressure;       		  	 /**< The pressure in hPA obtained from the BMP. It could be
     	 	 	 	 	 	 	 	 	  BMP_Client::InvalidPressure if sensor reading was impossible.
    									  Be sure to always test against this constant before using the value */
    float altitude;       			 /**< The altitude (m) as derived from the pressure obtained from the BMP.
    									  It could be BMP_Client::InvalidAltitude if sensor reading was impossible.
    									  Be sure to always test against this constant before using the value */
#ifdef INCLUDE_REFERENCE_ALTITUDE
    float refAltitude;       			 /**< The altitude (m) of the last detected stable altitude (assumed
     	 	 	 	 	 	 	 	 	  to be the altitude of the launching point. */
 #endif
 #ifdef INCLUDE_DESCENT_VELOCITY
    float descentVelocity;			 /**< The descent velocity in m/s, positive towards the ground. It could be
     	 	 	 	 	 	 	 	 	  BMP_Client::InvalidDescentVelocity if sensor reading was impossible.
    									  Be sure to always test against this constant before using the value */
#endif
    float temperatureThermistor1;    /**< The temperature in °C gathered from the first Thermistor*/
#ifdef INCLUDE_THERMISTOR2
    float temperatureThermistor2;    /**< The temperature in °C gathered from the second Thermistor*/
#endif
#ifdef INCLUDE_THERMISTOR3
    float temperatureThermistor3;    /**< The temperature in °C gathered from the third Thermistor*/
#endif
    // Secondary mission data to be added in the subclass

    /* Constants defining the the accuracy of float data. Warning: Do not increase beyond 1E6
    *  without changing the storage type (and the record binary size) accordingly !
    */
    static constexpr uint8_t NumDecimalPositions_Default=2;
    static constexpr uint8_t NumDecimalPositions_GpsPosition=5;
    static constexpr uint8_t NumDecimalPositions_Altitude=1;
    static constexpr uint8_t NumDecimalPositions_Temperature=1;
    static constexpr uint8_t NumDecimalPositions_Pressure=1;
    static constexpr uint8_t NumDecimalPositions_Velocity=2;
  protected:
    /** @name Protected methods to be overridden by projects-specific subclasses (ALL of them or none).
      *  @{   */
    // ------------------------------------------------------------------------------------------------
    // -----------   Methods below should be implemented in subclasses (ALL of them)  -----------------
    // ------------------------------------------------------------------------------------------------

    /** Print the secondary mission data in CSV format. Implementation guidelines: make use
     *  of the CansatRecord::printCSV utility methods, see CansatRecord::printCSV_Part for an example.
     *  @param str 				The stream to print to
     *  @param startWithSeparator 	If true tart the output with a separator before the first value.
     *  @param finalSeparator 	Include a finalSeparator if true
     */
    virtual void printCSV_SecondaryMissionData(Stream & str , bool /* startWithSeparator */, bool finalSeparator ) const {if (finalSeparator) str << separator;};

    /** Print the part of the header line regarding the secondary mission data in CSV format (ie.
     *  the name of each data, with a CansatRecord::separator as separator). The number of elements
     *  in the header must match the number of elements actually prints in the
     *  printCSV_SecondaryMissionData() method
     *  @param str 			The stream to print to
     *  @param startWithSeparator 	If true tart the output with a separator before the first value.
     *  @param finalSeparator 	Include a finalSeparator if true
     */
    virtual void printCSV_SecondaryMissionHeader(Stream & str, bool /* startWithSeparator */, bool finalSeparator ) const  {if (finalSeparator) str << separator;};

    /** Set all secondary mission data to a default "no-data" value.*/
    virtual void clearSecondaryMissionData() {};

    /** Obtain the maximum length of the part of the record data related to the secondary mission in CSV format (just in case include preceding and trailing comma) */
     virtual uint16_t getSecondaryMissionMaxCSV_Size() const { return 0;};

     /** Obtain the maximum length of the part of record header related to the secondary mission in CSV format (comma preceding headers INCLUDED) */
     virtual uint16_t getSecondaryMissionCSV_HeaderSize() const { return 0;} ;

    /** Print the secondary mission data in human-readable format (one data per line, in format
     *	"label (units) : value". Do not include a final end of line character.
     *	This method is only used for debugging purpose. Implementation should be consistent with implementation
     *	of method CansatRecord::printPart (in file CansatRecordGoodies.cpp.
     *  @param str 				The stream to print to
     */
    virtual void printSecondaryMissionData(Stream& /* str */) const {};

    /** Serialize the secondary mission data in binary format in the destination buffer.
	 *  It should be at least getSecondaryMissionBinarySize() bytes (this can safely be
	 *  assumed: it has been checked before this method is called).
	 *  Perform serialization by calling protected method writeBinary() on each data member.
	 *  If not writing the data with the full precision, be sure to handle rounding properly:
	 *  to round a float to an integer value, add 0.5 so truncation provides the closest
	 *  integer:
	 *  @code
	 *   	uint32_t integerValueInTenthOfUnit = (uint32_t) ((floatData * 10.0f)+0.5))
	 *  @endcode
	 *  (see usage information in method documentation, and an full example in
	 *  class CansatRecordExample).
	 *  @warning Be sure to update methods readBinarySecondaryMissionData() and
	 *  getBinarySizeSecondaryMissionData consistently whenever modifying this method.
	 *  @param destinationBuffer The buffer to write into
	 *  @param bufferSize The size of the provided destination buffer.
	 *  @return The number of bytes actually written into the buffer.
	 */
    virtual uint8_t writeBinarySecondaryMissionData(uint8_t* const /* destinationBuffer */, uint8_t /* bufferSize */) const { return 0;} ;

    /** Read the record in binary format from the source buffer.
     *  If the bufferSize is insufficient, an error is reported.
     *  Read by calling one of the protected methods readBinary() on each data member
	 *  (see usage information in method documentation, and an full example in
	 *  class CansatRecordExample).
     *  @param sourceBuffer The buffer to read from.
     *  @param bufferSize The size of the provided destination buffer.
     *  @return The number of byte actually read.
     */
    virtual uint8_t readBinarySecondaryMissionData(const uint8_t* const /* sourceBuffer */, const uint8_t /* bufferSize */) {return 0;};

    /** Obtain the size of the secondary mission data in binary format (as written by the writeBinarySecondaryMissionData() method */
    virtual uint8_t getBinarySizeSecondaryMissionData() const { return 0;};

	/** @} */  // End of group of methods to be overridden.
    // ------------------------------------------------------------------------------------------------
    // -----------------  Methods below should not be modified in subclasses --------------------------
    // ------------------------------------------------------------------------------------------------

    /** Utility function: print a float, using the right number of decimal positions, with or without a final separator.
     *  @param str 				The stream to print to
     *  @param f				The float value to print
     *  @param finalSeparator 	Include a finalSeparator if true (defaults to false)
     *  @param numDecimals		The number of decimal positions to use. It defaults to the number defined
     *  						in CansatConfig.h.
    */
    void printCSV(Stream& str, const float &f, bool finalSeparator = false, byte numDecimals = NumDecimalPositions_Default) const;

    /** Utility function: print a bool as 1 or 0, with or without a final separator.
     *  @param str 				The stream to print to
     *  @param b				The boolean value to print
     *  @param finalSeparator 	Include a finalSeparator if true (defaults to false)
    */
    void printCSV(Stream& str, const bool b, bool finalSeparator = false) const;

    /** Print a part of the record in CSV format
     *  @param str 				The stream to print to
     *  @param select			The part of the record to print (only relevant if headerOrContent is Header
     *  @param headerOrContent	Defines whether to print the header of part of the content of the record.
     *  @param finalSeparator 	Include a finalSeparator if true (defaults to false)
     */
    void printCSV_ExceptTimestamp(Stream & str, DataSelector select, HeaderOrContent headerOrContent, bool finalSeparator) const;

    /** Print a part of the record in human-readable format
     *  @param str 				The stream to print to
     *  @param select			The part of the record to print (only relevant if headerOrContent is Header
     */
    void printPart(Stream & str, DataSelector select) const;

    /** Write a trivially copyable value in buffer. The buffer pointer is incremented by the written size,
     *  the remaining size is decremented by the written size, and the written size is returned.
     *  @Usage
     *  @code
     *  		uint8_t* ptr=&myBuffer;
     *  		uint8_t	 remainingSize=myBufferSize;
     *  		uint8_t  written=0;
     *  		written+=writeBinary(ptr, remainingSize, value1);
     *  		written+=writeBinary(ptr, remainingSize, value2);
     *  		written+=writeBinary(ptr, remainingSize, value3);
     *  @endcode
     *  @param destPtr The address at which to write the value
     *  @param remaining The number of free bytes remaining from ptr. Should be at least sizeof(value)
     *  @param value The value to write. It must be a trivially copyable type.
     *  @return The number of bytes actually written.
     */
    template <class T>
    uint8_t writeBinary(uint8_t* &destPtr, uint8_t &remaining, const T value) const {
    	if (remaining < sizeof(T)) {
			DPRINTS(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, "CansatRecord::writeBinary: error. To write: ");
			DPRINT(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, sizeof(T));
			DPRINTS(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, ", remaining=");
			DPRINT(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, remaining);
			DPRINTSLN(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, " bytes");
			return 0;
    	}
    	memcpy(destPtr, &value, sizeof(T));
    	remaining-=sizeof(T);
    	destPtr+=sizeof(T);
		DPRINTS(DBG_CST_RECORD_TEMPLATE, "CansatRecord::writeBinary: wrote ");
		DPRINT(DBG_CST_RECORD_TEMPLATE, value);
		DPRINTS(DBG_CST_RECORD_TEMPLATE,  " (");
		DPRINT(DBG_CST_RECORD_TEMPLATE, sizeof(T));
		DPRINTSLN(DBG_CST_RECORD_TEMPLATE, " bytes)");
    	return sizeof(T);
    };

    /** Read a trivially copyable value from buffer. The buffer pointer is incremented by the read size,
	 *  the remaining size is decremented by the written size, and the read size is returned.
	 *  @Usage
	 *  Use in conjunction with the other version of this method
	 *  @code
	 *  		uint8_t* ptr=&myBuffer;
	 *  		uint8_t	 remainingSize=myBufferSize;
	 *  		uint8_t  read=0;
	 *  		uint16_t tmpValue;
	 *  		// Read value1, without transcoding
	 *  		read+=readBinary(ptr, remainingSize, value1);
	 *  		// Read value2 which is a float saved as integer after multiplication by 10:
	 *  		read+=readBinary(ptr, remainingSize, tmpValue, value2, 10.0);
	 *  		// Repeat as often as required.
	 *
	 *  @endcode
	 *  @param destPtr The address from which to read the value
	 *  @param remaining The number of free bytes left to read from ptr. Should be at least sizeof(value)
	 *  @param value The value to read. It must be a trivially copyable type.
	 *  @return The number of bytes actually read.
	 */
	template<class T>
	uint8_t readBinary(const uint8_t* &srcPtr, uint8_t &remaining, T &value) {
		if (remaining < sizeof(T)) {
			DPRINTS(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, "CansatRecord::readBinary1: error. To read: ");
			DPRINT(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, sizeof(T));
			DPRINTS(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, ", remaining=");
			DPRINT(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, remaining);
			DPRINTSLN(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, " bytes");
			return 0;
		}
		memcpy(&value, srcPtr, sizeof(T));
		remaining -= sizeof(T);
		srcPtr += sizeof(T);
		DPRINTS(DBG_CST_RECORD_TEMPLATE, "CansatRecord::readBinary1: read ");
		DPRINT(DBG_CST_RECORD_TEMPLATE, value);
		DPRINTS(DBG_CST_RECORD_TEMPLATE,  " (");
		DPRINT(DBG_CST_RECORD_TEMPLATE, sizeof(T));
		DPRINTSLN(DBG_CST_RECORD_TEMPLATE, " bytes)");
		return sizeof(T);
	};

	/** Read a trivially copyable value from buffer, applying a type conversion and a
	 *  scaling factor. The buffer pointer is incremented by the read size,
	 *  the remaining size is decremented by the written size, and the read size is returned.
	 *  @Usage
	 *  See other version of the method
	 *  @param destPtr The address from which to read the value
	 *  @param remaining The number of free bytes left to read from ptr. Should be at least sizeof(value)
	 *  @param value The value to read. It must be a trivially copyable type.
	 *  @param tmpValue The value to be read from binary before type conversion and scaling.
	 *  @param multiplicationFactor The multiplication factor applied before writing: the value
	 *  	   is divided by this factor (after type conversion).
	 *  @return The number of bytes actually read.
	 */
	template<class T, class I, class M>
	uint8_t readBinary(	const uint8_t* &srcPtr, uint8_t &remaining,
						I &tmpValue,
						T &value,
						M multiplicationFactor=1)  {
		if (remaining < sizeof(I)) {
			DPRINTS(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, "CansatRecord::readBinary2: error. To read: ");
			DPRINT(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, sizeof(I));
			DPRINTS(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, ", remaining=");
			DPRINT(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, remaining);
			DPRINTSLN(DBG_DIAGNOSTIC_CST_RECORD_TEMPLATE, " bytes");
			return 0;
		}
		memcpy(&tmpValue, srcPtr, sizeof(I));
		remaining -= sizeof(I);
		srcPtr += sizeof(I);
		value= ((T) tmpValue)/multiplicationFactor;
		DPRINTS(DBG_CST_RECORD_TEMPLATE, "CansatRecord::readBinary2: read ");
		DPRINT(DBG_CST_RECORD_TEMPLATE, tmpValue);
		DPRINTS(DBG_CST_RECORD_TEMPLATE,  " (");
		DPRINT(DBG_CST_RECORD_TEMPLATE, sizeof(I));
		DPRINTS(DBG_CST_RECORD_TEMPLATE, " bytes). Val=");
		DPRINTLN(DBG_CST_RECORD_TEMPLATE, value, 6);
		return sizeof(I);
	};

	/** Compute a float or double with an integer part which is the properly rounded
	 *  value of f*factor.
	 *  Examples: scaleFloat(10.6789, 100) returns 1068.xxxx
	 *  		  scaleFloat(-10.6789, 100) returns -1068.xxx
	 *  @param f The floating point number to scale and round.
	 *  @param factor The scaling factor
	 *  @return f*factor, with integer part properly rounded
	 */
	template<class T>
	inline T scaleFloat(const T& f, const T& factor ) const {
		return (f*factor)+0.5-(f<0);
	};

    static constexpr char separator = ',';
    friend class CansatRecord_Test;
} ;
