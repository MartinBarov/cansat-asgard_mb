/*
   BMP_Client.cpp
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "BMP_Client.h"
#define DBG_VELOCITY 0
#define DBG_MOVING_AVERAGE 0
#define DBG_BMP_TIMING 0
#define DBG_REFERENCE_ALITITUDE_CALCULATION 0
#define DBG_BMP_INIT 1
#define DBG_DIAGNOSTIC 1

BMP_Client::BMP_Client(): bmp(), seaLevelPressure(0.0) {
#ifdef INCLUDE_DESCENT_VELOCITY
  currentIdx = 0;
  for (int i = 0; i < BMP_NumPeriodsForDescentVelocityCalculation; i++) {
    previousAltitudes[i] = -1000;
    previousTimestamp[i] = 0;
  }
  for (int i = 0; i < BMP_NumDescentVelocitySamplesToAverage; i++) {
    previousDescentVelocity[i] = 0;
  }
  velocitySum = 0;
  currentVelocityIdx = 0;
#endif
}

bool BMP_Client::begin(float theSeaLevelPressureInHPa) {
#ifdef BMP_USE_BMP3XX_MODEL
  DPRINTSLN(DBG_BMP_INIT, "Initializing BMP3XX");
  if (!bmp.begin_I2C()) return false;
  bmp.setTemperatureOversampling(BMP3_OVERSAMPLING_8X); //(defaults to BMP3_NO_OVERSAMPLING)
  bmp.setPressureOversampling(BMP3_OVERSAMPLING_16X); //(defaults to BMP3_NO_OVERSAMPLING)
  bmp.setIIRFilterCoeff(BMP3_IIR_FILTER_DISABLE);
  //(defaults to BMP3_IIR_FILTER_DISABLE) Do not use IIR filter to avoid degrading the response time.
  bmp.setOutputDataRate(BMP3_ODR_50_HZ);
  //(defaults to BMP3_ODR_25_HZ)
#else
  DPRINTSLN(DBG_BMP_INIT, "Initializing BMP280");
  if (!bmp.begin()) return false;
#endif

  seaLevelPressure = theSeaLevelPressureInHPa;
  DPRINTS(DBG_BMP_INIT, "BMP: Sea-level pressure (hPa)=");
  DPRINTLN(DBG_BMP_INIT, theSeaLevelPressureInHPa);
  DPRINTS(DBG_BMP_INIT, "     Samples for pressure reading=");
  DPRINTLN(DBG_BMP_INIT, BMP_NumSamplesForPressureReading);
#ifdef INCLUDE_VELOCITY_CALCULATION
  DPRINTS(DBG_BMP_INIT, "     Periods for desc. velocity calculation:");
  DPRINTLN(DBG_BMP_INIT, BMP_NumPeriodsForDescentVelocityCalculation);
  DPRINTS(DBG_BMP_INIT, "     Desc. velocity samples averaged:");
  DPRINTLN(DBG_BMP_INIT, BMP_NumDescentVelocitySamplesToAverage);
#endif
  return true;
}

float BMP_Client::getPressure(uint8_t numSamples) {
  float result = 0;
  for (int i = 0; i < numSamples; i++) {
#ifdef BMP_USE_BMP3XX_MODEL
    if (! bmp.performReading()) {
      DPRINTSLN(DBG_DIAGNOSTIC, "BMP failed to perform reading");
      return InvalidPressure;
    }
    else {
      result += bmp.pressure;
    }
#else
    result += bmp.readPressure();
#endif
  }
  result /= 100.0;
  result /= numSamples;

  if (result < MinRealisticPressure) return InvalidPressure;
  else return result;
}

float BMP_Client::convertPressureToAltitude(float pressureInHPa)
{
  if (pressureInHPa == InvalidPressure) {
	  return InvalidAltitude;
  }
  float altitude = 44330 * (1.0 - pow(pressureInHPa / seaLevelPressure, 0.1903));
  if (isnan(altitude)) {
    return InvalidAltitude;
  }
  return altitude;
}

float BMP_Client::getAltitude() {
  return convertPressureToAltitude(getPressure());
}

float BMP_Client::getTemperature() {
#ifdef BMP_USE_BMP3XX_MODEL
  if (! bmp.performReading()) {
    DPRINTSLN(DBG_DIAGNOSTIC, "BMP failed to perform reading");
    return InvalidTemperature;
  }
  else {
    return bmp.temperature;
  }
#else
  return bmp.readTemperature();
#endif
}

float BMP_Client::computeDescentVelocity(const CansatRecord& record) {
	if (record.altitude == InvalidAltitude) {
	  return InvalidDescentVelocity;
	}

#if DBG_VELOCITY == 1
  for (int i = 0; i < BMP_NumPeriodsForDescentVelocityCalculation; i++) {
    Serial << i << ": " << previousTimestamp[i] << ": " << previousAltitudes[i] << ENDL;
  }
  Serial << "currentIdx = " << currentIdx << ENDL;
#endif

  float velocity = -2000;

  if (previousAltitudes[currentIdx] > -1000) {
    uint32_t deltats = record.timestamp - previousTimestamp[currentIdx];
    float deltaAltitude = previousAltitudes[currentIdx] - record.altitude;
    velocity = deltaAltitude / deltats;
    velocity *= 1000.0; // Convert from m/msec into m/s

  } else {
    velocity = -1000;
  }
  previousTimestamp[currentIdx] = record.timestamp;
  previousAltitudes[currentIdx] = record.altitude;
  currentIdx++ ;
  if ( currentIdx > (BMP_NumPeriodsForDescentVelocityCalculation - 1)) {
    currentIdx = 0;
  }
  DPRINTS(DBG_VELOCITY, "velocity (no avg)=");
  DPRINTLN(DBG_VELOCITY, velocity);
  return velocity;
}

float BMP_Client::computeMovingAverage(float velocity) {
#if DBG_MOVING_AVERAGE == 1
  for (int i = 0; i < BMP_NumDescentVelocitySamplesToAverage; i++) {
    Serial << i << ": velocity=" << previousDescentVelocity[i] <<  ENDL;
  }
  Serial << "currentVelocityIdx=" << currentVelocityIdx << ENDL;
  Serial << "velocitySum=" << velocitySum << ENDL;
  Serial << "velocity=" << velocity << ENDL;
#endif

  velocitySum += velocity;
  velocitySum -= previousDescentVelocity[currentVelocityIdx];
  previousDescentVelocity[currentVelocityIdx] = velocity;
  float v = velocitySum / BMP_NumDescentVelocitySamplesToAverage;
  currentVelocityIdx++;
  if ( currentVelocityIdx > (BMP_NumDescentVelocitySamplesToAverage - 1)) {
    currentVelocityIdx = 0;
  }

  DPRINTS(DBG_MOVING_AVERAGE, "velocity avg=");
  DPRINTLN(DBG_MOVING_AVERAGE, v);
  return v;
}

float BMP_Client::computeAveragedDescentVelocity(const CansatRecord& record) {
  float v = computeDescentVelocity(record);
  if (v == InvalidDescentVelocity) {
	  return InvalidDescentVelocity;
  } else {
	  return computeMovingAverage(v);
  }
}

bool BMP_Client::readData(CansatRecord& record) {

#if DBG_BMP_TIMING == 1
  uint32_t tsStart = millis();
#endif
  record.pressure = getPressure();
#if DBG_BMP_TIMING == 1
  uint32_t tsPressure = millis();
#endif
  record.altitude = convertPressureToAltitude(record.pressure);
#if DBG_BMP_TIMING == 1
  uint32_t tsAltitude = millis();
#endif
#ifdef BMP_USE_BMP3XX_MODEL
  record.temperatureBMP = bmp.temperature; // performReading() was already called by getPressure()
#else
  record.temperatureBMP = getTemperature();
#endif

#if DBG_BMP_TIMING == 1
  uint32_t tsTemperature = millis();
  Serial << "Reading durations (msec): pressure=" << tsPressure - tsStart
         << ", altitude=" << tsAltitude - tsPressure
         << ", temperature=" << tsTemperature - tsAltitude << ENDL;
#endif

#ifdef INCLUDE_DESCENT_VELOCITY
  record.descentVelocity = computeAveragedDescentVelocity(record);
#endif

#ifdef INCLUDE_REFERENCE_ALTITUDE
  updateReferenceAltitude(record);
#endif

  return true;
}

#ifdef INCLUDE_REFERENCE_ALTITUDE
void BMP_Client::updateReferenceAltitude(CansatRecord& record) {
	// If invalid altitude, set previous value and return immediately.
	if(record.altitude == BMP_Client::InvalidAltitude) {
		record.refAltitude = refAltitude;
		return;
	}

	if(record.timestamp - lastCallToUpdateReferenceAltitude > 5*CansatAcquisitionPeriod
		|| maxAltitudeInLastCycle - minAltitudeInLastCycle > NoAltitudeChangeTolerance) {
		// Not been called for too long or variation too large, reset measurement cycle

		DPRINTSLN(DBG_REFERENCE_ALITITUDE_CALCULATION, "Reference altitude measurement cycle reset.");

		refAltitudeCycleBegin = record.timestamp;

		minAltitudeInLastCycle = record.altitude;
		maxAltitudeInLastCycle = record.altitude;
	}
	else {
		minAltitudeInLastCycle = min(minAltitudeInLastCycle, record.altitude);
		maxAltitudeInLastCycle = max(maxAltitudeInLastCycle, record.altitude);

		if(record.timestamp - refAltitudeCycleBegin >= NoAltitudeChangeDurationForReset) {
			// Cycle elapsed
			refAltitude = (minAltitudeInLastCycle + maxAltitudeInLastCycle)/2;
			DPRINTS(DBG_REFERENCE_ALITITUDE_CALCULATION, "Reference altitude set to "); DPRINTLN(DBG_REFERENCE_ALITITUDE_CALCULATION, refAltitude);

			refAltitudeCycleBegin = record.timestamp;

			minAltitudeInLastCycle = record.altitude;
			maxAltitudeInLastCycle = record.altitude;
		}
	}

	record.refAltitude = refAltitude;
	lastCallToUpdateReferenceAltitude = record.timestamp;
}
#endif
