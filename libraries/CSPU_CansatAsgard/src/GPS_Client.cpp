// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "GPS_Client.h" //includes the .h
#ifdef NEW_VERSION
#  ifdef ARDUINO_ARCH_SAMD
#  include "SAMD_InterruptTimer.h"
#  endif
#else
#include "TC_Timer.h"
#endif

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG 1
#define DBG_NMEA 0
#define DBG_DIAGNOSTIC 1
#define DBG_FREQUENCY 1
#define DBG_TIMER_INFO 1

#ifndef NEW_VERSION
#define GPS_CLIENT_TIMER 5 // valid values are: 3, 4 and 5 for SAMD21
						   // 3 seems to be used by the core functions, 4 is used by the Servo library.
#endif

constexpr bool GPSECHO = false; // Set to true to echo whatever the GPS sends on Serial

#define PMTK_SET_NMEA_OUTPUT_GGAONLY "$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29" //This sentence doesn't appear in the official adafruit library but still gets accepted by the gps

static Adafruit_GPS *gpsForInterrupt = NULL;

#ifdef __AVR__ // interruption routine supported by __AVR__ architectures (arduino uno)
GPS_Client::GPS_Client(SoftwareSerial& serialPort): myGps(&serialPort), lastLongitudeValue(0), lastLatitudeValue(0), lastAltitudeValue(0)  {}
/**A double constructor is required as the SAMD architecture doesn't accept a softwareserial */
SIGNAL(TIMER0_COMPA_vect) {
  char c = gpsForInterrupt->read();
  // if you want to debug, this is a good time to do it!
  if (GPSECHO)
    if (c) UDR0 = c;
  // writing direct to UDR0 is much much faster than Serial.print
  // but only one character can be written at a time.
}
#elif defined(ARDUINO_ARCH_SAMD)
/* Constructor and interruption routine supported by the ATSMAD21G18 (feather m0 express)
   or ATSAMD51 Cortex M4 (ItsyBitsy M4 (NEW_VERSION only for M4 */
GPS_Client::GPS_Client(HardwareSerial& serialPort): myGps(&serialPort), lastLongitudeValue(0), lastLatitudeValue(0), lastAltitudeValue(0) {}

#ifdef NEW_VERSION
// This is the version that should work for both M0 (SAMD21) and M4 (SAMD51) boards.
static SAMD_InterruptTimer gpsTimer;

void TimerHandler() {
	char c = gpsForInterrupt->read();
    // if you want to debug, this is a good time to do it!
	if (GPSECHO) {
	   if (c) Serial.print(c);
	}
}
#else

#define CONCAT2(a, b) a ## b
#define CONCAT(a, b) CONCAT2(a, b)
static TC_Timer gpsTimer(CONCAT(TC_Timer::HwTimer::timerTC, GPS_CLIENT_TIMER));
void CONCAT(TC, CONCAT(GPS_CLIENT_TIMER, _Handler))()
{
  if (gpsTimer.isYourInterrupt()) {
    char c = gpsForInterrupt->read();
    // if you want to debug, this is a good time to do it!
    if (GPSECHO) {
      if (c) Serial.print(c);
    }
    gpsTimer.clearInterrupt(); // clear the interrupt so that it will run again
  }
}
#endif
#else
#error "UNSUPPORTED ARCHITECTURE IN GPS_Client.cpp"
#endif


void GPS_Client::begin(const Frequency updateFrequency)
{
  myGps.begin(9600); /**Sets an initialization baud rate for the gps. Tests show that it doesn't need to be changed in order to acquire data at 10HZ if using a GGAONLY NMEA sentence
                      WARNING: the baud rate sent to the gps, even if it doesn't accept it, will be conserved,
                      even if changing the code and uploading again, the only way to go back is to plug the gps out, losing the fix  */
  delay(1000); /**Gives time to the GPS to boot perfectly */
  gpsForInterrupt = &myGps;

#ifdef __AVR__
  // Timer0 is already used for millis() - we'll just interrupt somewhere
  // in the middle and call the "Compare A" function above
  OCR0A = 0xAF;
  TIMSK0 |= _BV(OCIE0A); //macro
#elif defined(ARDUINO_ARCH_SAMD)
#else
#error "UNSUPPORTED BOARD IN GPS_Client.cpp" /**If any of the architectures supported by the class isn't used (__AVR__ or feather m0) */
#endif

  myGps.sendCommand(PMTK_SET_NMEA_OUTPUT_GGAONLY);
  /** This sets the GPS to output only GGA sentences:
    the default setting for the communication between the board and the GPS chip can accomodate at most 960 bytes of data if transmitting permanently.
    Taking into account a few different formatting characters and some flow interrupts, it is not safe to go more than 800 char/s.
    As a consequence:
     10 . RMC = 650 bytes => accepted
     10 . (RMC + GGA) = 1300 bytes => impossible
     10 . GGA = 650 bytes => working
  */

  changeFrequency(updateFrequency);
  /** Sets output rate of the GPS to desired frequency (passed in as parameter) */
}

void GPS_Client::changeFrequency(const Frequency updateFrequency) {
  switch (updateFrequency) {
    case Frequency::F1Hz :
      myGps.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
      DPRINTLN(DBG_FREQUENCY, "GPS update frequency set to 1hz")
      break;
    case Frequency::F5Hz :
      myGps.sendCommand(PMTK_SET_NMEA_UPDATE_5HZ);
      DPRINTLN(DBG_FREQUENCY, "GPS update frequency set to 5hz")
      break;
    case Frequency::F10Hz :
      myGps.sendCommand(PMTK_SET_NMEA_UPDATE_10HZ);
      DPRINTLN(DBG_FREQUENCY, "GPS update frequency set to 10hz")
      break;
    default :
      DPRINTLN(DBG, "Unexpected frequency value. Terminating program.");
      DASSERT (false);
  }
#if defined(ARDUINO_ARCH_SAMD)
#  ifdef NEW_VERSION
   uint32_t interruptPeriod=1000/ ((int) updateFrequency*100);
   if (!gpsTimer.changePeriod(interruptPeriod))
   {
	   // This happens when the timer was never started before
	   if (gpsTimer.start(interruptPeriod, TimerHandler))
	   {
		   DPRINTS(DBG_TIMER_INFO, "Timer period set to ")
		   DPRINT(DBG_TIMER_INFO, interruptPeriod);
		   DPRINTSLN(DBG_TIMER_INFO, " msec");
	   }
	   else {
		   DPRINTSLN(DBG_DIAGNOSTIC, "ERROR: COULD NOT SET TIMER FOR GPS");
	   }
   }
#  else
  gpsTimer.configure((int)updateFrequency * 100);
  gpsTimer.enable();
  DPRINTS(DBG_TIMER_INFO, "Timer frequency set to ")
  DPRINT(DBG_TIMER_INFO, ((int)updateFrequency * 100));
  DPRINTSLN(DBG_TIMER_INFO, " Hz");
#  endif

#endif
}

void GPS_Client::readData(CansatRecord & record)
{
  record.newGPS_Measures = false;
  bool newReceived = myGps.newNMEAreceived();
  if (newReceived) {
    DPRINTLN(DBG_NMEA, myGps.lastNMEA());
    bool result = myGps.parse(myGps.lastNMEA());
    if (!result) {
      DPRINTSLN(DBG_DIAGNOSTIC, "ERROR PARSING NMEA");
    }
  }

  // Valid fix are 1 (GPS fix), 2 (DGPS fix), 3 (PPS fix)
  bool fixOK = ((myGps.fixquality == 1) || (myGps.fixquality == 2)  || (myGps.fixquality == 3));
  if (fixOK && newReceived) {
    DPRINTSLN(DBG_NMEA, "fix ok in in NMEA");
    record.newGPS_Measures = true;
    record.GPS_LongitudeDegrees = myGps.longitudeDegrees;
    record.GPS_LatitudeDegrees = myGps.latitudeDegrees;
    lastLongitudeValue = myGps.longitudeDegrees;
    lastLatitudeValue = myGps.latitudeDegrees;
    lastAltitudeValue = myGps.altitude;
#ifdef INCLUDE_GPS_VELOCITY
    record.GPS_VelocityKnots = myGps.speed;
    record.GPS_VelocityAngleDegrees = myGps.angle;
#endif
    record.GPS_Altitude = myGps.altitude;
  }
  else {
    //DPRINTSLN(DBG_NMEA, "fix not ok in in NMEA or no new data");
    record.GPS_LongitudeDegrees = lastLongitudeValue;
    record.GPS_LatitudeDegrees = lastLatitudeValue;
    record.GPS_Altitude = lastAltitudeValue;

  }
}
