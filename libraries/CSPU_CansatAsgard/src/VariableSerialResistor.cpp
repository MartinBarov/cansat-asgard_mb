/*
   VariableSerialResistor.cpp
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "VariableSerialResistor.h"


#define DBG_READ  0
#define DBG_READ_AVG  0

#ifdef ARDUINO_AVR_UNO
constexpr uint16_t Nsteps = 1023;
#elif defined(ARDUINO_ARCH_SAMD)
constexpr uint16_t Nsteps = 4095;
#else
#error "Board not recognized in VariableSerialResistor"
#endif

static constexpr int NumAverageSamples = 50; // Number of samples of the voltage averaged at each measurement.

VariableSerialResistor::VariableSerialResistor(const float theVcc, const uint8_t theAnalogPinNbr, const float theKnownResistor) {
  knownResistor = theKnownResistor;
  analogPinNbr = theAnalogPinNbr;
  vcc = theVcc;
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
  analogReadResolution(12);
#endif
}

float VariableSerialResistor::readResistance()const {
  float V = 0.0;
  for (int i = 0; i < NumAverageSamples; i++)
  {
    int sensorValue = analogRead(analogPinNbr);
    V += sensorValue * (vcc / Nsteps);
    DPRINTS(DBG_READ_AVG, "   SensorValue=");
    DPRINT(DBG_READ_AVG, sensorValue);
    DPRINTS(DBG_READ_AVG, ", V=");
    DPRINTLN(DBG_READ_AVG, V);
  }
  V /= NumAverageSamples;
  float R = (V * knownResistor) / (vcc - V);
  DPRINTS(DBG_READ, "numSamples=");
  DPRINT(DBG_READ, NumAverageSamples);
  DPRINTS(DBG_READ, ", V=");
  DPRINT(DBG_READ, V);
  DPRINTS(DBG_READ, ", serial resistor=");
  DPRINT(DBG_READ, knownResistor);
  DPRINTS(DBG_READ, ", Vcc=");
  DPRINT(DBG_READ, vcc);
  DPRINTS(DBG_READ, ", Nsteps=");
  DPRINT(DBG_READ, Nsteps);
  DPRINTS(DBG_READ, ", R=");
  DPRINTLN(DBG_READ, R);

  return (R);
}
