/*
 * Serial2.cpp
 */

#if defined(ARDUINO_ARCH_SAMD)
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "Serial2.h"
#include "wiring_private.h"

SercomSerial1 Serial2;

#  ifndef __SAMD51__
// This is the version for SAMD21 (tested on Adafruit Feather M0 Express)
SercomSerial1::SercomSerial1()
  : Uart(&sercom1,  11, 10 ,  SERCOM_RX_PAD_0 , UART_TX_PAD_2), rx(11), tx(10) {};

void SercomSerial1::begin(unsigned long baudRate) 
{
  Uart::begin(baudRate);
  pinPeripheral(rx, PIO_SERCOM); // Pins 10/11 must be configured to use the Sercom Mux
  pinPeripheral(tx, PIO_SERCOM); // Warning: this cannot be done in the constructor!
}

// Interrupt request handler
void SERCOM1_Handler()
{
  Serial2.IrqHandler();
}

#  else
// This is the version for SAMD51 (tested on Adafruit ItsyBitsy M4)
#    define SAMD51_USE_SERCOM4
#    ifdef SAMD51_USE_SERCOM3
   // This version causes conflict on  Adafruit ItsyBitsy M4 because Sercom3 is already
   // in use.
SercomSerial1::SercomSerial1()
  : Uart(&sercom3,  11, 12 ,  SERCOM_RX_PAD_3 , UART_TX_PAD_2), rx(11), tx(12) {};

void SercomSerial1::begin(unsigned long baudRate)
{
  Uart::begin(baudRate);
  pinPeripheral(rx, PIO_SERCOM_ALT); // Pins 10/11 must be configured to use the Sercom Mux
  pinPeripheral(tx, PIO_SERCOM); 	 // Warning: this cannot be done in the constructor!
}

void SERCOM3_0_Handler()
{
  Serial2.IrqHandler();
}
void SERCOM3_1_Handler()
{
  Serial2.IrqHandler();
}
void SERCOM3_2_Handler()
{
  Serial2.IrqHandler();
}
void SERCOM3_3_Handler()
{
  Serial2.IrqHandler();
}
#    elif defined(SAMD51_USE_SERCOM4)

SercomSerial1::SercomSerial1()
  : Uart(&sercom4,   A3, A2, SERCOM_RX_PAD_1, UART_TX_PAD_0), rx(A3), tx(A2) {};

void SercomSerial1::begin(unsigned long baudRate)
{
  Uart::begin(baudRate);
  pinPeripheral(rx, PIO_SERCOM_ALT); // Pins 10/11 must be configured to use the Sercom Mux
  pinPeripheral(tx, PIO_SERCOM_ALT); 	 // Warning: this cannot be done in the constructor!
}

void SERCOM4_0_Handler()
{
  Serial2.IrqHandler();
}
void SERCOM4_1_Handler()
{
  Serial2.IrqHandler();
}
void SERCOM4_2_Handler()
{
  Serial2.IrqHandler();
}
void SERCOM4_3_Handler()
{
  Serial2.IrqHandler();
}
#    endif  // USE SERCOMn
#  endif // SAMD51
#endif  // architecture
