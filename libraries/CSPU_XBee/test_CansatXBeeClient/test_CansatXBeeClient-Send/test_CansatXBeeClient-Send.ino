/**
 Test for the emitting part of CansatXBeeClient.h (using the Can-side XBee module)
 Test with Feather M0 board.

 The contents of the record and testString are defined in TestCansatRecord.h
 (because it is shared by the emitter and receiver sketches.

 Wiring: see test_CansatXBeeClient-Send sketch.

 Instructions: TO BE COMPLETED

 */

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#  error "This program only works on Feather MO_Express"
#endif

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG 1
#include "CansatConfig.h"
#include "CansatXBeeClient.h"
#include "CansatRecordExample.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

HardwareSerial &RF = Serial1;
CansatXBeeClient xbc(GroundXBeeAddressSH, GroundXBeeAddressSL); // Defined in CansatConfig.h
CansatRecordExample myRecord;
char myString[xbc.MaxStringSize];
CansatFrameType stringType;
bool isRecord;

void setup() {
	Serial.begin(115200);
	while (!Serial) {
		;
	}
	digitalWrite(LED_BUILTIN, HIGH);
	Serial << ENDL << ENDL;
	Serial << "***** EMITTER SKETCH (testing CansatXBeeClient) *****" << ENDL;
	Serial << "Initializing Serials and communications..." << ENDL;
	RF.begin(115200);
	xbc.begin(RF); // no return value

	myRecord.initValues();

	Serial << "Initialisation over. Test record content (size = " << sizeof(myRecord)<< " bytes):" << ENDL;
	myRecord.print(Serial);
	Serial << ENDL << "Test string : " << ENDL;
	Serial << "'" << CansatRecordExample::testString << "' (length=" << strlen(CansatRecordExample::testString) << ")" << ENDL;
	Serial << "-------------------" << ENDL << ENDL;
}

void loop() {
	if (xbc.send(myRecord)) {
		Serial << "Sent record" << ENDL;
	} else {
		Serial << "Error sending record" << ENDL;
		myRecord.print(Serial);
	}
	delay(1000);

	xbc.openStringMessage(CansatRecordExample::TestStringType, CansatRecordExample::SequenceNumber);
	xbc << CansatRecordExample::testString;
	if (xbc.closeStringMessage()) {
		Serial << "Sent test string" << ENDL;
	} else {
		Serial << "Error sending test string " << ENDL;
	}
	delay(1000);

	// Testing performance
	elapsedMillis ts=0;
	for (int i = 0; i < 10; i++) {
		if (!xbc.send(myRecord)) {
			Serial << "Error sending record" << ENDL;
		}
	}
	uint32_t duration=ts;
	Serial << "Sent 10 records in " << ts << " msec (without ack), ie. " << ts/10.0 << " msec/record" << ENDL << ENDL;
	delay(1000);
}
