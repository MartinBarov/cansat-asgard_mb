// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatRecordExample.h"
#include "elapsedMillis.h"
#include "string.h"
#include "CansatXBeeClient.h"
#include "CansatConfig.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

// Configuration constants
constexpr unsigned long emissionPeriod = 100; // msec.
constexpr unsigned long randomRange=emissionPeriod/10;
constexpr byte RF_RxPinOnUno = 9;
constexpr byte RF_TxPinOnUno = 11;
//define SIMULATE_ON_USB_SERIAL // Define to have RF reception and transmission performed on Serial (for debugging)
//define PRINT_IGNORED_STRING   // Define to output the strings received from RF and discarded.
#define CMD_STRING "5,12345,67890"
constexpr unsigned long RF_BaudRate = 115200; // This must be consistent with the configuration of the RF modules. 9600 is just enough!
constexpr unsigned long cmdPeriod = 15000; // msec
constexpr unsigned long responseDelay = 2000; // msec
constexpr bool showBufferOverlow = true; // Set to true to get messages on Serial when the reception buffer overflows.
constexpr bool showBufferWheneverProcessed = false ; // Set to true to display the buffer content each time it is processed.
constexpr bool showIgnoredMsg = true; // Set to true to display ignored (valid) incoming messages.
constexpr bool showStateChanges = true; // Set to true to display every state change.
constexpr bool printAllCharactersReceived = false; // Set to true to have every single char received from the RF immediately printed on Serial
constexpr bool showInvalidMsg = true; // Set to true to display all invalid messages received.

// -----------------------------------------------------------------------------------------
//Define a RF Serial port.
#ifdef SIMULATE_ON_USB_SERIAL
auto &RF = Serial;
#else
#  ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#   else
#   include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#   endif
#endif
//Types-------------------------------------------------------------------------------------
typedef enum MsgType {
  None,
  Record,
  CmdAck,
} MsgType_t;

// The states
typedef enum State {
  ReceivingRecords,
} State_t;

//Globals-----------------------------------------------------------------------------------
elapsedMillis elapsed, heartbeatElapsed;
CansatRecordExample outgoingRecord, incomingRecord;
CansatXBeeClient xbc(GroundXBeeAddressSH, GroundXBeeAddressSL); // Defined in CansatConfig.h
long randomizedPeriod=emissionPeriod;

//Declarations of methods--------------------------------------------------------------------
bool checkRecord(CansatRecordExample& rec) {
  bool dataOK = rec.checkValues(true);
  static unsigned long currentTS = 0; // the timestamp expected in next record.

  bool tsOK = false;
  if (currentTS == 0) {
    currentTS = rec.timestamp + 1;
    tsOK = true;
  } else {
    if (rec.timestamp == currentTS) {
      tsOK = true;
    } else {
      Serial << "Error in timestamp: expected " << currentTS << ", got " << rec.timestamp << ENDL;
    }
    // Serial << "setting currentTS to " << rec.timestamp + 1;
    currentTS = rec.timestamp + 1;
  }
  return (tsOK && dataOK);
}

void sendOneRecord () {
  static int counter = 0;
  counter++;
  xbc.send(outgoingRecord);
  outgoingRecord.timestamp++;
  Serial << ".";
  if (counter >= 100) {
    counter = 0;
    Serial << ENDL;
  }
}

//Main--------------------------------------------------------------------------------
void setup() {
  //DINIT_IF_PIN(115200, SerialActivationPin);
  DINIT(115200);
  Serial << F(" Using XBee pair ") << RF_XBEE_MODULES_SET << ENDL;
  Serial << F("=== Simulating the Can ===") << ENDL;
  Serial << F("=== Sending and recieving messages ===") << ENDL;
#ifdef SIMULATE_ON_USB_SERIAL
  Serial << F(" === Using USB Serial instead of RF Serial for test! ===") << ENDL;
#else
  RF.begin(RF_BaudRate);
#endif
  xbc.begin(RF);

  pinMode(LED_BUILTIN, OUTPUT);
  outgoingRecord.initValues();
  outgoingRecord.timestamp = 100;

  elapsed = 0;
  Serial << F("This is the record which is transferred:") << ENDL;
  outgoingRecord.print(Serial);
  Serial << ENDL;
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "Feather board detected: using RX-TX pins for RF communication" << ENDL;
#else
  Serial << "Assuming AVR board: using softwareSerial (rx=" << RF_RxPinOnUno << ", tx=" << RF_TxPinOnUno
         << ") for RF communication" << ENDL;
#endif
  Serial << F("RF baud rate   : ") << RF_BaudRate << F(" baud") << ENDL;

  Serial << F("Record emission period=") << emissionPeriod << F(" msec") << ENDL;
  Serial << ENDL << F("Setup complete.") << ENDL;
  Serial << F("A dot is a sent message from this RelayCan, to the ground.") << ENDL;
  Serial << F("A r is a received message from the SubCan.") << ENDL;
  Serial << F("This CanRelay also recieves messages form the SubCan.") << ENDL;
}

void loop() {

  //Send-----------------------------------------------------------------------------------------
  if (elapsed >= randomizedPeriod)
  {
    randomizedPeriod= emissionPeriod + random(randomRange) - randomRange/2;
    elapsed = 0;
    sendOneRecord();
    
  }
  
  //Receiving--------------------------------------------------------------------------------
  bool gotRecord;
  char incomingString[xbc.MaxStringSize + 1];
  CansatFrameType stringType;
  uint8_t seqNbr;
  // WARNING: DO NOT USE A while() IN NEXT LINE. It does not work.
  if (xbc.receive(incomingRecord, incomingString, stringType, seqNbr, gotRecord)) {
    if (gotRecord) {
      //Serial<< "got "<< incomingRecord.timestamp << ENDL;
      if (checkRecord(incomingRecord)) {
        Serial << "r";
      } else {
        Serial << "I";
      }
    } else {
      switch (stringType) {
        case CansatFrameType::CmdResponse:


          Serial << "Received '" << incomingString
                 << "' CmdResponse (ignored)" << ENDL;

          break;
        case CansatFrameType::CmdRequest:
          Serial << "Received unexpected '" << incomingString
                 << "' CmdRequest (ignored)" << ENDL;
          break;
        default:
          Serial << "***Error: unexpected string type: "
                 << (int) stringType << ENDL;
      }//switch
    } // else
  }//while
}
