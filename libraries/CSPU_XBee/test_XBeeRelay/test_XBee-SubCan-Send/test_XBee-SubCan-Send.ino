
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatRecordExample.h"
#include "elapsedMillis.h"
#include "string.h"
#include "CansatXBeeClient.h"
#include "CansatConfig.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

// Configuration constants
constexpr unsigned long emissionPeriod = 100; // msec.
constexpr byte RF_RxPinOnUno = 9;
constexpr byte RF_TxPinOnUno = 11;
//define SIMULATE_ON_USB_SERIAL // Define to have RF reception and transmission performed on Serial (for debugging)
//define PRINT_IGNORED_STRING   // Define to output the strings received from RF and discarded.
#define CMD_STRING "5,12345,67890"
#define RSP_STRING "** Command received **"
constexpr unsigned long RF_BaudRate = 115200; // This must be consistent with the configuration of the RF modules. 9600 is just enough!

// -----------------------------------------------------------------------------------------
//Define a RF Serial port.
#ifdef SIMULATE_ON_USB_SERIAL
auto &RF = Serial;
#else
#  ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#   else
#   include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#   endif
#endif

CansatRecordExample outgoingRecord;
CansatXBeeClient xbc(0x0013a200, 0x418FBBEB);

elapsedMillis elapsed;

void sendOneRecord()
{
  static int counter = 0;
  counter++;
  xbc.send(outgoingRecord);
  outgoingRecord.timestamp++;
  Serial << ".";
  if (counter >= 100) {
    counter = 0;
    Serial << ENDL;
  }
}

void setup() {
  //DINIT_IF_PIN(115200, SerialActivationPin);
  DINIT(115200);
  while (!Serial) ;
  Serial << F(" Using XBee pair ") << RF_XBEE_MODULES_SET << ENDL;
  Serial << F("=== Simulating a SubCan ===") << ENDL;
#ifdef SIMULATE_ON_USB_SERIAL
  Serial << F(" === Using USB Serial instead of RF Serial for test! ===") << ENDL;
#else
  RF.begin(RF_BaudRate);
#endif
  xbc.begin(RF);

  pinMode(LED_BUILTIN, OUTPUT);
  outgoingRecord.initValues();
  outgoingRecord.timestamp = 100;

  elapsed = 0;
  Serial << F("This is the record which is transferred:") << ENDL;
  outgoingRecord.print(Serial);
  Serial << ENDL;
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "Feather board detected: using RX-TX pins for RF communication" << ENDL;
#else
  Serial << "Assuming AVR board: using softwareSerial (rx=" << RF_RxPinOnUno << ", tx=" << RF_TxPinOnUno
         << ") for RF communication" << ENDL;
#endif
  Serial << F("RF baud rate   : ") << RF_BaudRate << F(" baud") << ENDL;

  Serial << F("Record emission period=") << emissionPeriod << F(" msec") << ENDL;
  Serial << ENDL << F("Setup complete") << ENDL;
  Serial << F("A dot is a sent message from the Subcan") << ENDL;
}


void loop() {

  // Sending always performed with a complete record.
  if (elapsed >= emissionPeriod)
  {
    elapsed = 0;
    sendOneRecord();
  }
}
