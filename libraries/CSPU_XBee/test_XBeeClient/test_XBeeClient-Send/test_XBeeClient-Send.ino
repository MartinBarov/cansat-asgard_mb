/**
 Test for the sending part of XBeeClient.h (using the Can-side XBee module, and
 assuming the receiving program uses the Ground station one)
 Test with Feather M0 board (should work with any SAMD-board...).
 Payload bytes are filled with consecutive integers from 0 to 255, so the
 receiver can easily check the content and detect strings (byte 0 > 0).
 Strings are "ABCD..." with type set to string length.

 Wiring: µC to XBee module (can-side).
   3.3V to VCC
   RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
   TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
   GND  to GND

 Instructions:
 - Be sure to configure the right XBee pair in CansatConfig.h and to wire the Can
   module to the board running the emitter sketch, and the Ground module to the
   board running the receiver sketch.
 - Run without receiving party (if should fail)
 - Run with receiving party (test_XBeeClient-Receive). For accurate performance testing,
   set SilentOnFrameReception to false in the emitter sketch.

   Activate outgoing and incoming frames tracing in XBeeClient if internal debugging needed
 */
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#  error "This program only works on SAMD boards"
#endif

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG 1
#include "CansatConfig.h"
#include "XBeeClient.h"


#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

HardwareSerial &RF = Serial1;
XBeeClient xbc(GroundXBeeAddressSH, GroundXBeeAddressSL); // Defined in CansatConfig.h
uint8_t payloadSize = 0;
uint8_t stringSize = 0;
uint8_t payload[XBeeClient::MaxPayloadSize] = { 0 };
enum TestType_t {
	binary, strings, performance
};
TestType_t testType = binary;
constexpr uint8_t numIterationsForPerformanceTest = 10;
constexpr uint8_t numBinaryTests = 5;
constexpr uint8_t numStringTests  = 3;


void setup() {
	Serial.begin(115200);
	while (!Serial) {
		;
	}
	digitalWrite(LED_BUILTIN, HIGH);

	Serial << ENDL << ENDL;
	Serial << "***** EMITTER SKETCH (testing XBeeClient) *****" << ENDL;
	Serial << "Initialising Serials and communications..." << ENDL;
	RF.begin(115200);
	xbc.begin(RF); // no return value
	Serial << "  Using module pair " << RF_XBEE_MODULES_SET <<";" << ENDL;
	Serial << "  Destination (ground): SH=0x";
	Serial.print(GroundXBeeAddressSH, HEX);
	Serial  << ", SL=0x";
	Serial.println(GroundXBeeAddressSL,HEX);
    Serial << "Using maximum payload size (" << XBeeClient::MaxPayloadSize << " bytes" << ENDL;

	uint8_t counter = 0;
	for (int i = 0; i < XBeeClient::MaxPayloadSize; i++) {
		payload[i] = counter++;
		if (counter >= 255) {
			counter = 0;
		}
	}
	Serial << "Content of test frame: " << ENDL;
	xbc.displayFrame(payload, XBeeClient::MaxPayloadSize);

	Serial << "Initialisation over" << ENDL;
}

void sendBinary() {
	payloadSize += 10;
	if (payloadSize > XBeeClient::MaxPayloadSize)
		payloadSize = 10;

	bool result = xbc.send(payload, payloadSize, 500);
	if (result) {
		Serial << "Transmitted " << payloadSize << " bytes successfully" << ENDL;
	} else {
		Serial << "*** ERROR *** Error during transmission" << ENDL;
	}
}

void sendString() {
	stringSize += 10;
	if (stringSize > XBeeClient::MaxStringSize) stringSize = 10;
	Serial << "Preparing string with length " << stringSize << "..." << ENDL;
	xbc.openStringMessage(stringSize); // use length as type
	for (int i=0; i < stringSize; i++) {
		xbc << (char) ('A' + (i %26));
	}
	if (xbc.closeStringMessage()) {
		Serial << "Sent string with length and type " << stringSize << ENDL;
	} else {
		Serial << "*** Error sending string with length and type " << stringSize
				<< ENDL;
	}
}

void testPerformance() {
	Serial << "Running performance test..." << ENDL;
	elapsedMillis ts=0;
	for (int i=0; i < numIterationsForPerformanceTest; i++) {
		bool result = xbc.send(payload, xbc.MaxPayloadSize, 500);
		if (!result) {
			Serial << "*** ERROR *** Perf. test: Error during transmission" << ENDL;
		}
	}
	uint32_t duration=ts;
	Serial << "Transmitted " << numIterationsForPerformanceTest << " x " << xbc.MaxPayloadSize
		   << " bytes in " << duration << " msec (" << duration/numIterationsForPerformanceTest
		   << " msec/frame)." << ENDL;
}

void loop() {
	static uint8_t testCounter=0;
	switch (testType) {
	case binary:
		sendBinary();
		if (testCounter >= numBinaryTests) {
			 testCounter=0;
			 testType = strings;
			 Serial << ENDL << "--- String tests ---" << ENDL;
		}
		break;
	case strings:
		sendString();
		if (testCounter >= numStringTests) {
			 testCounter=0;
			 testType = performance;
			 Serial << ENDL << "--- Performance tests ---" << ENDL;
		}
		break;
	case performance:
		testPerformance();
		testCounter=0;
		testType=binary;
		 Serial << ENDL << "--- Binary tests ---" << ENDL;
		break;
	default:
		Serial << "*** ERROR: unexpected test type" << ENDL;
	}
    testCounter++;
	delay(1000);
}
