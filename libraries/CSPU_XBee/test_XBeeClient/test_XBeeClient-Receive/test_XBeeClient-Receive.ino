/**
 Test for the receiving part of XBeeClient.h (using the ground-side XBee module)
 Test with Feather M0 board (should work with any other SAMD board...)

 Wiring and instructions: see test_XBeeClient-Send sketch (but use the ground-side XBee module).

 */
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#  error "This program only works on SAMD boards"
#endif

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG 1
#include "CansatConfig.h"
#include "XBeeClient.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

constexpr bool SilentOnFrameReception=false;
HardwareSerial &RF = Serial1;
// Addresses are for the counter part.
XBeeClient xbc(CanXBeeAddressSH, CanXBeeAddressSL); // Defined in CansatConfig.h
uint8_t payloadSize;
uint8_t* buffer;
uint32_t counter = 0;

void setup() {
	Serial.begin(115200);
	while (!Serial) {
		;
	}
	digitalWrite(LED_BUILTIN, HIGH);
	Serial << "***** RECEIVER SKETCH (testing XBeeClient) *****" << ENDL;
	Serial << "Initialising Serials and communications..." << ENDL;
	RF.begin(115200);
	xbc.begin(RF); // no return value
	Serial << "  Using module pair " << RF_XBEE_MODULES_SET << ";" << ENDL;
	Serial << "  Destination (can): SH=0x";
	Serial.print(CanXBeeAddressSH, HEX);
	Serial << ", SL=0x";
	Serial.print(CanXBeeAddressSL, HEX);
	Serial << ENDL << "Initialisation over." << ENDL;
}

void loop() {
	counter++;
	if (xbc.receive(buffer, payloadSize)) {
		counter = 0;
		if (!SilentOnFrameReception) {
			Serial << "Received " << payloadSize << " bytes " << ENDL;
		}
		if (buffer[0] != 0)  { // This is a string
			char s[xbc.MaxPayloadSize];
			uint8_t sType;
      uint8_t seqNbr;
			xbc.getString(s, sType, seqNbr, buffer, payloadSize);
			Serial << "String type=" << sType << ", '" << s << "'" << ENDL;
		} else {
			 if (!SilentOnFrameReception) {
				 xbc.displayFrame(buffer, payloadSize);
			 }
		}
	}
	if (counter > 600000) {
		counter = 0;
		Serial << "Still alive (but not receiving anything)..." << ENDL;
	}
}
