/*
 * CansatXBeeClient_Utilities.cpp
 *
 * A couple of utility methods not required for operation. They are located in a separated
 * compilation unit to avoid unnecessary inclusion in the executable.
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatXBeeClient.h"

#ifdef RF_ACTIVATE_API_MODE

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_STREAM 0

void CansatXBeeClient::displayString(uint8_t* data, uint8_t dataSize) const
{
  CansatFrameType type = (CansatFrameType) data[0];
  const char* s = (const char*) (data + 2);
  switch (type) {
    case CansatFrameType::StatusMsg:
      Serial << "Status message carrying string: '" << s << "'" << ENDL;
      break;
    case CansatFrameType::CmdRequest :
      Serial << "Command Request message carrying string: '" << s << "'" << ENDL;
      break;
    case CansatFrameType::CmdResponse :
      Serial << "Command Response message carrying string: '" << s << "'" << ENDL;
      break;
    case CansatFrameType::StringPart :
      Serial << "String Part message carrying string: '" << s << "'" << ENDL;
      break;
    default:
      Serial << "*** Unsupported string type: " << (int) type << ENDL;
  }
  Serial << "length=" << strlen(s) << ", size=" << dataSize;
  if (strlen(s) + 3 != (size_t) (dataSize)) {
	Serial << ENDL << "*** Error: inconsistent size" << ENDL;
  }
  if ((strlen(s) > 0) && (s[strlen(s) - 1] == '\n')) {
	Serial << "(last character is an ENDL)";
  }
  Serial << ENDL;
}
#endif


