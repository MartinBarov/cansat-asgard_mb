#pragma once
#include "Arduino.h"
#include "IsatisConfig.h"

class IsatisDataRecord {
  public:
    void print(Stream& str) const;
    void printCSV(Stream& str, bool slow=false) const;
    void printCSV_Header(Stream& str) const;
 
    void clear();
    float BMP_Temperature;
    float BMP_Pressure;
    float BMP_Altitude;

    double GPY_OutputV;
    bool   GPY_Quality;
#ifndef USE_MINIMUM_DATARECORD
    double GPY_Voc;
    double GPY_DustDensity;
    double GPY_AQI;
#endif

    unsigned long startTimestamp;
    unsigned long endTimestamp;


} ;
