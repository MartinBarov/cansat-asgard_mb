#ifndef ARDUINO_ARCH_SAMD
#error "Incompatible board (SAMD21 processor required)."
#endif

#include "AsyncServoWinchSW55136MA.h"
#include "elapsedMillis.h"
#include "TC_Timer.h"

#define USE_ASSERTIONS
#define DEBUG_CSPU
#include "DebugCSPU.h"


TC_Timer myTimer(TC_Timer::HwTimer::timerTC5);


const byte PWM_Pin = 9;
const uint16_t motorSpeedToUse = 10; // (mm/s)

AsyncServoWinchSW55136MA servo;


const uint16_t maxRopeLen = 500;
const uint16_t minPulseWidth = 1000;
const uint16_t maxPulseWidth = 2000;

class ServoWinch_Test {
  public:
    uint16_t getRopeLenFromPulseWidth(uint16_t pulseWidth){
      return servo.getRopeLenFromPulseWidth(pulseWidth);
    }
    uint16_t getPulseWidthFromRopeLen(uint16_t ropeLen){
      return servo.getPulseWidthFromRopeLen(ropeLen);
    }
};

ServoWinch_Test testHelper;

void waitForUser(char msg[] = "Press any key to continue") {
  while (Serial.available() > 0) {
    Serial.read();
  }
  Serial.println(msg);
  Serial.flush();
  while (Serial.available() == 0) {
    delay(300);
  }
  Serial.read();
}


void setup() {
  Serial.begin(115200);
  while (!Serial) {}

  myTimer.configure(10);
  myTimer.enable();
  
  waitForUser("Press any key to start motor and fully extend (using ropeLength)");
  servo.begin(PWM_Pin, ServoWinch::ValueType::ropeLength, maxRopeLen, motorSpeedToUse);

  waitForUser("Press any key to go to middle (using pulse width) and overshoot by 2 cm");
  servo.setOvershootBy(20);
  servo.setTarget(ServoWinch::ValueType::pulseWidth, 1500);

  while(!servo.isAtTarget()) {delay(10);}
  waitForUser("Press any key to fully retract (using pulse width) and try to overshoot by 2 cm (won't actually overshoot)");
  servo.setTarget(ServoWinch::ValueType::pulseWidth, minPulseWidth);

  while(!servo.isAtTarget()) {delay(10);}
  waitForUser("Press any key to go to middle (using rope length) and overshoot by 3 cm");
  servo.setOvershootBy(30);
  servo.setTarget(ServoWinch::ValueType::ropeLength, maxRopeLen/2);

  while(!servo.isAtTarget()) {delay(10);}
  waitForUser("Press any key to fully retract (using rope length)");
  servo.setOvershootBy(0);
  servo.setTarget(ServoWinch::ValueType::ropeLength, 0);

  while(!servo.isAtTarget()) {delay(10);}
  waitForUser("RopeLen->PulseWidth\n------------------");
  for(int i = 0; i < maxRopeLen; i++) {
    Serial << i << "->" << testHelper.getPulseWidthFromRopeLen(i) << ENDL;
  }
  Serial << ENDL << ENDL;

  waitForUser("PulseWidth->RopeLen\n------------------");
  for(int i = minPulseWidth; i < maxPulseWidth; i++) {
    Serial << i << "->" << testHelper.getRopeLenFromPulseWidth(i) << ENDL;
  }
  Serial << ENDL << ENDL;

  
  waitForUser("Press any key to test precision of conversion angle/rope length");
  uint16_t doubleInverseOperation; uint16_t delta; bool ok;

  
  ok = false; delta = 0;
  while(!ok) {
    delta++; ok = true;
    for(int i = minPulseWidth; i <= maxPulseWidth; i++) {
      doubleInverseOperation = testHelper.getPulseWidthFromRopeLen(testHelper.getRopeLenFromPulseWidth(i));
      ok = i-delta <= doubleInverseOperation && doubleInverseOperation <= i+delta; if(!ok) break;
    }
  }
  Serial.print("Conversion pulseWidth->ropeLen->pulseWidth precise with delta = +-"); Serial.print(delta); Serial.println(" microseconds.");

  ok = false; delta = 0;
  while(!ok) {
    delta++; ok = true;
    for(int i = 0; i <= maxRopeLen; i++) {
      doubleInverseOperation = testHelper.getRopeLenFromPulseWidth(testHelper.getPulseWidthFromRopeLen(i));
      ok = i-delta <= doubleInverseOperation && doubleInverseOperation <= i+delta; if(!ok) break;
    }
  }
  Serial.print("Conversion ropeLen->pulseWidth->ropeLen precise with delta = +-"); Serial.print(delta); Serial.println(" mm.");

  waitForUser("Press any key to test speed of conversion");
  int tmp;
  float timeForOneConversion;
  elapsedMillis timeForAllConversions = 0;
  for(int i = minPulseWidth; i <= maxPulseWidth; i++) {
    tmp = testHelper.getRopeLenFromPulseWidth(i);
  }
  timeForOneConversion = timeForAllConversions*1000/(maxPulseWidth - minPulseWidth + 1);
  Serial.print("Conversion time pulseWidth->ropeLength averages "); Serial.print(timeForOneConversion, 2); Serial.println(" microseconds.");

  timeForAllConversions = 0;
  for(int i = 0; i <= maxRopeLen; i++) {
    tmp = testHelper.getPulseWidthFromRopeLen(i);
  }
  timeForOneConversion = timeForAllConversions*1000/(maxRopeLen + 1);
  Serial.print("Conversion time ropeLength->pulseWidth averages "); Serial.print(timeForOneConversion, 2); Serial.println(" microseconds.");


  waitForUser("Press any key to turn off motor (without moving)");
  servo.end();
}

void loop() {
  // put your main code here, to run repeatedly:

}

void TC5_Handler (void) {
  if (myTimer.isYourInterrupt()) {
    //Serial.println("tc5");
    servo.run();
    myTimer.clearInterrupt();
  }
}
