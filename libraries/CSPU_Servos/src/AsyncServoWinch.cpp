// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "AsyncServoWinch.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"

#define DBG 1

#define DBG_ASYNCSERVOSET 0
#define DBG_ASYNCSERVORUN 0
#define DBG_DIAGNOSTIC 1

bool AsyncServoWinch::begin(byte PWM_Pin, ValueType initPositionType, uint16_t initPosition, uint8_t mmPerMoveToUse) {
  if (running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call AsyncServoWinch::begin because the servo is already on.");
    return false;
  }

  if (!setTarget(initPositionType, initPosition)) return false; // Set target is important here, otherwise target variables are uninitialized
  elapsedSinceLastMove = 0;
  mmPerMove = mmPerMoveToUse;

  return ServoWinch::begin(PWM_Pin, initPositionType, initPosition);
}

bool AsyncServoWinch::end(bool moveBeforeEnding, ValueType endPositionType, uint16_t endPosition) {
  if (!running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call AsyncServoWinch::end because the servo is not on.");
    return false;
  }

  if (moveBeforeEnding) {
    if (!setTarget(endPositionType, endPosition)) return false;
    ending = true;
  }
  else {
    if (!ServoWinch::end()) return false;
    ending = false;
  }

  return true;
}

bool AsyncServoWinch::setTarget(ValueType type, uint16_t value) {
  if (ending) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call AsyncServoWinch::setTarget because the servo is currently powering off.");
    return false;
  }
  switch (type) {
    case ValueType::pulseWidth:
      if(value == targetPulseWidth) return true; // (same target as before)
      if (value < minPulseWidth || maxPulseWidth < value) {
        DPRINTLN(DBG_DIAGNOSTIC, "Value provided to ServoWinch::setTarget is out of bounds. Ignoring command.");
        return false;
      }
      targetPulseWidth = value;
      targetRopeLen = getRopeLenFromPulseWidth(value);
      break;

    case ValueType::ropeLength:
      if(value == targetRopeLen) return true; // (same target as before)
      if (maxRopeLen < value) {
        DPRINTLN(DBG_DIAGNOSTIC, "Value provided to AsyncServoWinch::setTarget is out of bounds. Ignoring command.");
        return false;
      }
      targetPulseWidth = getPulseWidthFromRopeLen(value);
      targetRopeLen = value;
      break;

    default:
      DPRINTLN(DBG, "Unexpected ValueType value. Terminating program.");
      DASSERT (false);
      return false;
  }

  // calculate effective overshootTargetBy
  if (inputOvershootTargetBy == 0 || targetRopeLen == currRopeLen) {
    overshootTargetBy = 0;
  }
  else if (targetRopeLen < currRopeLen) {
    overshootTargetBy = -min(inputOvershootTargetBy, targetRopeLen);
  }
  else if (targetRopeLen > currRopeLen) {
    overshootTargetBy = min(inputOvershootTargetBy, maxRopeLen - targetRopeLen);
  }

  DPRINT(DBG_ASYNCSERVOSET, "Setting servo-motor target pulse width to ");
  DPRINTLN(DBG_ASYNCSERVOSET, targetPulseWidth);

  return true;
}

void AsyncServoWinch::run() {
  if (currRopeLen == targetRopeLen + overshootTargetBy) {
    overshootTargetBy = 0;
  }

  if (ending && isAtTarget()) {
    delay(5000);
    end();
  }
  else if (running && elapsedSinceLastMove >= updateFrequency) {
    DPRINTLN(DBG_ASYNCSERVORUN, "Updating servo-motor angle if necessary.");
    elapsedSinceLastMove = 0;
    if(!isAtTarget()) {
      if (currRopeLen > targetRopeLen + overshootTargetBy) set(ValueType::ropeLength, max(targetRopeLen + overshootTargetBy, currRopeLen - mmPerMove));
      else if (currRopeLen < targetRopeLen + overshootTargetBy) set(ValueType::ropeLength, min(targetRopeLen + overshootTargetBy, currRopeLen + mmPerMove));
      tsTargetReached = millis();
    }
  }
}

void AsyncServoWinch::setOvershootBy(uint16_t overshootTargetByToUse) {
  inputOvershootTargetBy = overshootTargetByToUse;
}

uint16_t AsyncServoWinch::getTargetRopeLen() const {
  return targetRopeLen;
}

uint16_t AsyncServoWinch::getTargetPulseWidth() const {
  return targetPulseWidth;
}

bool AsyncServoWinch::isAtTarget() const {
  return currRopeLen == targetRopeLen /* at target */ && overshootTargetBy == 0 /* Overshooting finished */;
}

unsigned long AsyncServoWinch::elapsedSinceTargetReached() {
  if(isAtTarget()) return millis() - tsTargetReached;
  else return 0;
}
