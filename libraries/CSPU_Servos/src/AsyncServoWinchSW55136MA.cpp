// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "AsyncServoWinchSW55136MA.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"

#define DBG 1

AsyncServoWinchSW55136MA::AsyncServoWinchSW55136MA() : AsyncServoWinch(SW55136MA_MaxRopeLen, SW55136MA_MinPulseWidth, SW55136MA_MaxPulseWidth) {}

constexpr uint16_t AsyncServoWinchSW55136MA::SW55136MA_RopeLenToPulseWidth[];

uint16_t AsyncServoWinchSW55136MA::getRopeLenFromPulseWidth(uint16_t pulseWidth) const {
  DASSERT(minPulseWidth <= pulseWidth && pulseWidth <= maxPulseWidth);
  
  if(pulseWidth > SW55136MA_RopeLenToPulseWidth[getConversionTableSize() - 1]) {
    return map(pulseWidth, SW55136MA_RopeLenToPulseWidth[getConversionTableSize() - 1], maxPulseWidth, (getConversionTableSize() - 1)*10, maxRopeLen);
  }
  
  uint8_t begin = 0;
  uint8_t end = getConversionTableSize() - 1;
  uint8_t mid;

  while (begin + 1 < end) {
    mid = (begin + end) / 2;
    if (SW55136MA_RopeLenToPulseWidth[mid] == pulseWidth) return mid * 10;
    else if (SW55136MA_RopeLenToPulseWidth[mid] < pulseWidth) begin = mid;
    else end = mid;
  }

  return map(pulseWidth, SW55136MA_RopeLenToPulseWidth[begin], SW55136MA_RopeLenToPulseWidth[end], begin * 10, end * 10);
}

uint16_t AsyncServoWinchSW55136MA::getPulseWidthFromRopeLen(uint16_t ropeLen) const {
  DASSERT(ropeLen <= maxRopeLen);

  if (ropeLen % 10 == 0 && ropeLen / 10 < getConversionTableSize()) return SW55136MA_RopeLenToPulseWidth[ropeLen / 10];
  else if (ropeLen / 10 < getConversionTableSize() - 1) return map(ropeLen, ropeLen - ropeLen % 10, ropeLen + 10 - ropeLen % 10, SW55136MA_RopeLenToPulseWidth[(ropeLen - ropeLen % 10)/10], SW55136MA_RopeLenToPulseWidth[(ropeLen + 10 - ropeLen % 10)/10]);
  else return map(ropeLen, (getConversionTableSize() - 1)*10, maxRopeLen, SW55136MA_RopeLenToPulseWidth[getConversionTableSize() - 1], maxPulseWidth);
}

uint8_t AsyncServoWinchSW55136MA::getConversionTableSize()  {
  return sizeof(SW55136MA_RopeLenToPulseWidth) / sizeof(SW55136MA_RopeLenToPulseWidth[0]);
}
