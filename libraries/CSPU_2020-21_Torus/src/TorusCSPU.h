/*
 * TorusCSPU.h 
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup TorusCSPU 
 * in the class documentation block.
 */

 /** @defgroup TorusCSPU TorusCSPU library 
 *  @brief The library of classes specific to the CanSat 2020/2021 Torus project.
 *  
 *  The TorusCSPU library contains all the code for the Torus project, which is assumed not to be reused accross project
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - TimeCSPU
 *  - elapsedMillis
 *  - cansatAsgardCSPU
 *  @todo TO BE COMPLETED.
 *  
 *  
 *  _History_\n
 *  The library was created by the 2020/2021 Cansat team (Torus) based on the IsatisCSPU, IsaTwo and SpySeeCan libraries.
 */
