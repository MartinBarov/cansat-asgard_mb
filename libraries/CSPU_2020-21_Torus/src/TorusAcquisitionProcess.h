/*
 * TorusAcquisitionProcess.h
 */

#pragma once
#include "CansatAcquisitionProcess.h"
#include "TorusHW_Scanner.h"
#include "TorusRecord.h"
#include "TorusSecondaryMissionController.h"

/**
 *  @ingroup TorusCSPU
 *  @brief The class implementing features specific to the Torus project:
 *  	- The secondary mission data acquiring
 *  	- The secondary mission control
 */
class TorusAcquisitionProcess: public CansatAcquisitionProcess {
public:
	TorusAcquisitionProcess() {};
	virtual ~TorusAcquisitionProcess() { }	;
	
	/** Obtain the TorusHW_Scanner initialized by the process in method initHW_Scanner()
	 */
	virtual TorusHW_Scanner* getHardwareScanner() override {
		return (TorusHW_Scanner*) CansatAcquisitionProcess::getHardwareScanner();
	}
	;

private:
	/** Return a pointer to an instance of a TorusHW_Scanner. It can be overridden by subclasses to provide
	 *  an instance of an appropriate subclass of HardwareScanner.
	 *  The object is NOT initialised: its init() method will be called during the AcquisitionProcess'
	 *  initialization phase.
	 *  The object is dynamically allocated. Ownership of the object is transferred and it will be
	 *  released when the CansatAcquisitionProcess is released.
	 */
	virtual TorusHW_Scanner* getNewHardwareScanner() override {
		return new TorusHW_Scanner();
	}
	;

    /** Return a pointer to an instance of a TorusRecord. It is overridden to provide
       *  an instance of an appropriate subclass of CansatRecord.
       *  The record should be dynamically allocated. Ownership of the object is transferred and it will be
       *  released when the CansatAcquisitionProcess is released.
       */
	virtual TorusRecord* getNewCansatRecord() override { return new TorusRecord;};
	
	/** Initialize the specific components for the Torus project (the secondary mission controller)
	*/
	virtual void initSpecificProject() override {
		secondaryMissionCtrl.begin(getHardwareScanner()->getServoWinch());
	};
	
	/** Acquire the Torus secondary mission data and run the Torus secondary mission controller.
	*/
	virtual void acquireSecondaryMissionData (CansatRecord &theRecord) override {
		secondaryMissionCtrl.run((TorusRecord&)theRecord);
	};
	
	TorusSecondaryMissionController secondaryMissionCtrl; /**< The Torus secondary mission controller object. */
};

