#pragma once
#include "AsyncServoWinchSW55136MA.h"
#include "TorusRecord.h"
#include "TorusConfig.h"

/** @ingroup TorusCSPU
    @brief Subclass of AsyncServoWinchSW55136MA that inplements writing to the TorusRecord. When begin or end are called, the servo will return to the position defined in the TorusConfig.h (servoInitAndEndPosition)
    @todo Decide whether motor should be turned off during takeoff
*/

class TorusServoWinch : public AsyncServoWinchSW55136MA {
  public:
    TorusServoWinch();

    /** Start the servo-motor and set it to the initial length. Call this before calls to any other method.
      @param PWM_Pin The PWM pin used by the motor
      @param mmPerMoveToUse When moving servo motor, each updateFrequency milliseconds, rope will be extended/retracted by this amount of mm
    */
    bool begin(byte PWM_Pin=ServoWinchPWM_PinNbr, uint8_t mmPerMoveToUse=ServoWinch_mmPerMove);

    /** Change the position of the motor to the initial length and power off the motor. If you want to use the motor later, you will have to call begin again.
    */
    bool end();

    bool setTarget(ValueType type, uint16_t value) override;

    /** Reset the target position to the intial position */
    bool resetPosition();

    /** Write necessary data to record
      @param record A reference to the TorusRecord in which the data should be written
    */
    void readData(TorusRecord &record);
};
