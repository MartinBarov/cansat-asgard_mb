// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "TorusServoWinch.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"

#define DBG 1
#define DBG_TORUSSERVOREADDATA 1
#define DBG_DIAGNOSTIC 1

TorusServoWinch::TorusServoWinch() : AsyncServoWinchSW55136MA() {
  // initializing currRopeLen and targetRopeLen in case readData is called before call to begin
  switch (ServoInitAndEndPositionType) {
    case ValueType::pulseWidth :
      currPulseWidth = ServoInitAndEndPosition;
      currRopeLen = getRopeLenFromPulseWidth(ServoInitAndEndPosition);
      break;
    case ValueType::ropeLength :
      currPulseWidth = getPulseWidthFromRopeLen(ServoInitAndEndPosition);
      currRopeLen = ServoInitAndEndPosition;
      break;
  }
  targetPulseWidth = currPulseWidth;
  targetRopeLen = currRopeLen;
}

bool TorusServoWinch::begin(byte PWM_Pin, uint8_t mmPerMoveToUse) {
  if (running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call TorusServoWinch::begin because servo is already on.");
    return false;
  }
  return AsyncServoWinchSW55136MA::begin(PWM_Pin, ServoInitAndEndPositionType, ServoInitAndEndPosition, mmPerMoveToUse);
}

bool TorusServoWinch::resetPosition() {
  if (!running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call TorusServoWinch::resetPosition() because the servo is not on.");
    return false;
  }
  return setTarget(ServoInitAndEndPositionType, ServoInitAndEndPosition);
}

bool TorusServoWinch::end() {
  if (!running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call TorusServoWinch::end because the servo is not on.");
    return false;
  }
  return AsyncServoWinchSW55136MA::end(true, ServoInitAndEndPositionType, ServoInitAndEndPosition);
}

bool TorusServoWinch::setTarget(ValueType type, uint16_t value) {
  switch (type) {
    case ValueType::ropeLength :
      if(value == targetRopeLen) return true; // (same target as before)
      if (value < currRopeLen) setOvershootBy(ServoOvershootWhenRetracting);
      else setOvershootBy(ServoOvershootWhenExtending);
      break;
    case ValueType::pulseWidth :
      if(value == targetPulseWidth) return true; // (same target as before)
      if (value < currPulseWidth) setOvershootBy(ServoOvershootWhenRetracting);
      else setOvershootBy(ServoOvershootWhenExtending);
      break;
    default :
      DPRINTLN(DBG, "Unexpected ValueType value. Terminating program.");
      DASSERT (false);
      return false;
  }

  return AsyncServoWinchSW55136MA::setTarget(type, value);
}
void TorusServoWinch::readData(TorusRecord & record) {
  record.parachuteRopeLen = currRopeLen;
  record.parachuteTargetRopeLen = targetRopeLen;
}
