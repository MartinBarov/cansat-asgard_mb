/*
 * TestUtilityFunctions.h
 *
 */

#pragma once
#include "TorusRecord.h"
#include "TorusSecondaryMissionController.h"

extern uint32_t numErrors;
extern uint32_t recordCounter;
extern TorusSecondaryMissionController controller;
extern bool stopAfterFailedTest;
extern bool stopAfterFirstFailedCheck;

/** process the record, updating altitude, increasing timestamp by CansatAcquisitionPeriod
 *  and superimposing +-0.75m of noise on the altitude.
 */
void RunA_RecordForRefAltitudeTest(TorusRecord& record,float altitude);

/** Print secondary mission -related information from record */
void printSecondaryInfo(TorusRecord&rec);

/** Check controller info in record against expected values. Phase is not checked if 255 */
void checkCtrlInfo(TorusRecord& record,TorusControlCode ctrlCode, const char* ctrlCodeMsg, uint8_t phase=255) ;

/** Check whether flight phase is either phase1 or phase2 (used to support phase detectiong delay) */
void checkPhase(TorusRecord& record, uint8_t phase1, uint8_t phase2) ;

/** Check target rope len is the target value.
 *  This function keeps track of the previous target, and does not fail if the target is still
 *  the previous one during TorusMinDelayBetweenTargetUpdate msec.  */
void checkTarget(TorusRecord& record, uint16_t target) ;

/** Alternately add an substract 1.1*NoAltitudeChangeTolerance to altitude, in order to avoid
 *  the can is considered static.
 */
void wobbleAltitude(TorusRecord& record);

/** Just wait until the board is running since at least TorusMinDelayBeforeFirstUpdate msec. */
void waitEndOfStartup() ;

/** Process enough times the record (increaing timestamp) for the reference altitude to be set to the current altitude */
void setReferenceAltitude(TorusRecord& record);

/** Print short summary of errors so far. Stop program if StopAfterFailedTest is true */
void printTestSummary();
