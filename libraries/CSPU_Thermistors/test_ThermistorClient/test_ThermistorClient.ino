/*
   test_ThermistorClient
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "ThermistorClient.h"
#include "CansatRecord.h"
#include "BMP_Client.h"
#include "ThermistorNTCLE100E3.h"
#include "ThermistorNTCLG100E2104JB.h"
#include "ThermistorVMA320.h"

constexpr bool csvFormat = true;
constexpr float Vcc=3.3;
constexpr byte NTCLE_pin=A0;
constexpr byte NTCLG_pin=A1;
constexpr byte VMA_pin=A2;
constexpr uint32_t NTCLE_Resistor=33000;
constexpr uint32_t NTCLG_Resitor=18000;
constexpr uint32_t VMA_Resistor=10000;

ThermistorNTCLE100E3 		thermistor1(Vcc, NTCLE_pin, NTCLE_Resistor);
ThermistorNTCLG100E2104JB 	thermistor2(Vcc, NTCLG_pin, NTCLG_Resitor);
ThermistorVMA320			thermistor3(Vcc, VMA_pin, VMA_Resistor);
ThermistorClient 			therm;
uint32_t counter=0;
uint8_t numberOfThermistors=0; // Valid values: 0, 1, 2, 3

BMP_Client bmp;
CansatRecord record;

void setup() {
  DINIT(115200);
  if (csvFormat) {
    Serial << "time (seconds),BMP280, NTCLE100E3, NTCLG100E2, VMA320" << ENDL;
  }
  bmp.begin(1037.6);
  Serial << "BMP OK"<< ENDL;
  if (therm.readData(record) !=0) {
	  Serial << "Error: readData with no thermistor configured. It should return 0! " << ENDL;
	  while(1);
  } else Serial << "Testing without Thermistor: OK!" << ENDL;
  therm.setThermistors(&thermistor1);
}

void loop() {
  record.clear();
  counter++;
  switch(numberOfThermistors) {
  case 0:
	  Serial << "Error: 0 thermistor configured (in loop). Aborted" << ENDL;
	  exit(1);
	  break;
  case 1:
	  if (counter > 3) {
		  therm.setThermistors(&thermistor1,&thermistor2);
		  numberOfThermistors=2;
		  Serial << "Configured second thermistor..." << ENDL;
	  }
	  break;
  case 2:
	  if (counter > 6) {
		  therm.setThermistors(&thermistor1,&thermistor2,&thermistor3);
		  numberOfThermistors=3;
		  Serial << "Configured third thermistor..." << ENDL;
	  }
	  break;
  default:
	  break;
  }
  
  bool result = therm.readData(record);
  if (!result) {
    Serial << "*** error while reading temperature from the thermistors" << ENDL;
  }
  result = bmp.readData(record);
  if (!result) {
    Serial << "*** error while reading temperature from the bmp" << ENDL;
  }

  if (csvFormat) {
    Serial << millis() / 1000.0 << "," << record.temperatureBMP << " ," << record.temperatureThermistor1 << " ," << record.temperatureThermistor2 << " , " << record.temperatureThermistor3 << ENDL;
  }
  else
    record.print(Serial);

  delay(5000);

}
