/** @file
 *  @brief This file defines a small #Timer class, allowing for timing methods or block execution.
*/

#pragma once


#include "elapsedMillis.h"
/** @ingroup CSPU_Debug
 * @brief  A small utility class to easily time method or block execution.
 * 
   Usage:
      @li define symbol USE_TIMER
      @li Either: \n
          Just declare a Timer variable as local variable in the block to be timed. 
          The constructor starts a timer and the destructor reads and outputs it on the Serial interface.\n
        OR (recommanded): \n
          Use it through the DBG_TIMER macro to preserve a 0 memory-footprint in the operational software.
      @li Embedded timers are supported (and nicely displayed using indentation).
*/
class Timer {
  public:
    Timer(__FlashStringHelper* msg);
    ~Timer();

  protected:
    static byte level; // The embedding level.
    elapsedMillis internalTimer;
    __FlashStringHelper* location;
};

#ifdef USE_TIMER
#   define DBG_TIMER(blockName)  Timer _dbg_Timer((__FlashStringHelper*) F(blockName)); 
    // NB: __FUNCTION__ is gcc-specific 
#else
/** @ingroup CSPU_Debug
 * @brief   Time the execution of the block containing the DBG_TIMER(xxx) instruction, provided the debugging is enabled and symbol USE_TIMER is defined.
 * Usage:
 * @code
 *    #define USE_TIMER
 *    #include "Timer.h"
 *    {
 *      DBG_TIMER("nameOfMyBlockOrMethod");
 *      (... actual code to time .... )
 *    }
 * @endcode
 */
#  define DBG_TIMER(blockName)
#endif
