/* 
 *  Functions used for debugging. Do not make conditional to DEBUG_CSPU:
 *  Library is compiled into a .a and actually linked if used only. 
 *  Keep this file minimal to avoid memory usage when debugging is off. 
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "DebugCSPU.h"

/** @brief Return the number of free bytes in dynamic memory.
 *  This function has a ZERO memory foot-print. 
 */
#ifndef USING_SIMULATED_ARDUINO
#  ifdef ARDUINO_ARCH_SAMD
     extern "C" char *sbrk(int i);
     int freeRam () {  
       char stack_dummy = 0;
       return &stack_dummy - sbrk(0);
     }
#  else
     // This is the default version for Arduino Uno
     int freeRam () {
       extern int __heap_start, *__brkval;
       int v;
       return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
     }
#  endif
#else
    return 0;
#endif

void dumpMemory(const uint8_t* data, size_t len)
{
	size_t i;
	Serial << "Dumping " << len << " bytes from address Ox";
	Serial.println( (uint32_t) data, HEX);
	for (i=1;i<=len;i++) {
		Serial << data[i] << " ";
		if (i%10==0) Serial << ENDL;
	}
	Serial << ENDL;
}
