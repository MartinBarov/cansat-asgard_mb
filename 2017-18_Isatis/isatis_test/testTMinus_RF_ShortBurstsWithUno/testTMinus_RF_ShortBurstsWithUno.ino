
/*
    This sketch emits numbered cycles of integers on the RF link.
    It is intended to be run on an Arduino Uno, connected to a
    T-Minus RF board:
      Vcc, Gnd connected
      RF board's pin 14 connected to Arduino Uno's pin 9
      RF board's pin 13 connected to Arduino Uno's pin 8

*/

#include <SoftwareSerial.h>

const int RF_BaudRate = 19200;
const int blinkDelay  = 100; // msec on, then off.
const byte RX_Pin = 8;
const byte TX_Pin = 9;

SoftwareSerial gtSerial(RX_Pin, TX_Pin); // Arduino RX, Arduino TX

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  while (!Serial);
  Serial.println(F("Init OK for serial link."));

  gtSerial.begin(RF_BaudRate);  // software serial port
  Serial.print(F("Baud rate for communication to RF board: "));
  Serial.println(RF_BaudRate);
  Serial.print(F("TX to RF board is on Uno's pin #"));
  Serial.println(TX_Pin);

  // Conclusion of Init
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.println(F("Starting emission..."));
  Serial.println(F("---------------------------------"));
}


void loop() {
  // put your main code here, to run repeatedly:
  static  int i = 0;
  Serial.println(i);

  while (gtSerial.available()) {
    gtSerial.read();
    // Just discard any possible incoming data.
  }
  gtSerial.print(" RF ");
  gtSerial.println(i);

  digitalWrite(LED_BUILTIN, LOW);
  delay(blinkDelay);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(blinkDelay);

  i++;
}
