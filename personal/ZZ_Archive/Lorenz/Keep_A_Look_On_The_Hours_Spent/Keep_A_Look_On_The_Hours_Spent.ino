/*
    Program to have keep a constant eye on such things like
    the hours of work per student during the CanSat_Project
*/
#include <EEPROM.h>
#define Debug
#include "DebugCSPU.h"

// Addresses
int addressLolo = 1;
int addressAline = 2;
int addressMaud = 3;
int addressLiya = 4;
int addressRemi = 5;
int addressAlex = 6;

char tutorial;
bool tuto;
char initialsWorker;
char nameWorker;
int action;
byte resultAction1;

int tutorialFunction() {
  Serial << "Do you want a tutorial ([y]es/[n]o) ?" << ENDL;
  tutorial = Serial.read();
  if (tutorial == "y") {

  }
}
void exitFunction() {
  exit(0);
}

char whoAreYou() {
  // Introduction
  Serial << "First of all, decline your identity" << "Write your initials if your name is in the following list: " << ENDL;

  // List
  Serial << "[L]orenz [V]eithen" << ENDL;
  Serial << "[A]line [J]acquart" << ENDL;
  Serial << "[M]aud [T]oussaint" << ENDL;
  Serial << "[L]iya [R]osenstein" << ENDL;
  Serial << "[R]emi [K]omino" << ENDL;
  Serial << "[A]lexandre [R]oybin" << ENDL;
  Serial << "" << ENDL;
  Serial << "Write now: ";
  initialsWorker = Serial.read();
  Serial << initialsWorker << ENDL;
  if (nameWorker != "LV" && nameWorker != "AJ" && nameWorker != "MT" && nameWorker != "LR" && nameWorker != "RK" && nameWorker != "AR") {

    // How can I do a register by myself ?
    Serial << "*** Error: not registered yet! Contact Lorenz Veithen" << ENDL;
    return;
  }
  return nameWorker;
}

char fullNameWorker(char initialsWorker) {
  if (initialsWorker == "LV") {
    nameWorker = "Lorenz Veithen";
    return nameWorker;
  }
  if (initialsWorker == "AJ") {
    nameWorker = "Aline Jacquart";
    return nameWorker;
  }
  if (initialsWorker == "MT") {
    nameWorker = "Maud Toussaint";
    return nameWorker;
  }
  if (initialsWorker == "LR") {
    nameWorker = "Liya Rosenstein";
    return nameWorker;
  }
  if (initialsWorker == "RK") {
    nameWorker = "Remi Komino";
    return nameWorker;
  }
  if (initialsWorker == "AR") {
    nameWorker = "Alexandre Roybin";
    return nameWorker;
  }
}

int whatDoYouWantToDo() {
  Serial << "Nice to see you" << nameWorker << ENDL;
  Serial << "What do you want to do ?" << "Write the number correspunding to your request in the following list: " << ENDL;
  Serial << "(1) See the amount of hours devoted by yourself to the CanSat Project"  << ENDL;
  Serial << "(2) Enter additional hours of work" << ENDL;
  Serial << "(3) begin a clock for several people working together" << ENDL; // Not sure about how to do it...
  Serial << "" << ENDL;

  Serial << "Write now...";
  action = Serial.read();
  Serial << action << ENDL;

  return action;
}

void doingAction1(char initialsWorker) {

  Serial << "Doing action 1" << ENDL;
  Serial << "Result of action 1: ";
  switch (initialsWorker) {
    case 'LV':
      resultAction1 = EEPROM.read(addressLolo);
      Serial << resultAction1 << ENDL;
      break;

    case 'AJ':
      resultAction1 = EEPROM.read(addressAline);
      Serial << resultAction1 << ENDL;
      break;

    case 'MT':
      resultAction1 = EEPROM.read(addressMaud);
      Serial << resultAction1 << ENDL;
      break;

    case 'LR':
      resultAction1 = EEPROM.read(addressLiya);
      Serial << resultAction1 << ENDL;
      break;

    case 'RK':
      resultAction1 = EEPROM.read(addressRemi);
      Serial << resultAction1 << ENDL;
      break;

    case 'AR':
      resultAction1 = EEPROM.read(addressAlex);
      Serial << resultAction1 << ENDL;
      break;
  }
}

void doingAction2(char initialsWorker) {
  Serial << "Doing action 2" << ENDL;

  Serial << "Enter how many hours of work you just completed following the next example: " << ENDL;
  Serial << "";
       }

         void setup() {
         // Welcome

         Serial << "Welcome on this program !" << ENDL;
         initialsWorker = whoAreYou();
         nameWorker = fullNameWorker(initialsWorker);
         action = whatDoYouWantToDo();
         if (action == 1) {
         doingAction1(initialsWorker);
       }
         if (action == 2) {

       }
         if (action == 3) {

       }
       }

         void loop() {
         delay(500);
       }
