/*
    Test program for the use of the XBee API mode through streaming operators.
*/

#define RF_ACTIVATE_API_MODE  // If undefined, transparent mode is used. 
#include "IsaTwoXBeeClient.h"

IsaTwoXBeeClient xb(0x0013a200, 0x418fb90a);
IsaTwoRecord rec;

void initRecord(IsaTwoRecord& rec) {
  rec.timestamp = 12345;
  rec.accelRaw[0] = 3;  rec.accelRaw[1] = 4;  rec.accelRaw[2] = 5;
  rec.GPS_LongitudeDegrees = 98.765;
  rec.GPS_LatitudeDegrees = 34.56789;
  rec.pressure = 1013.1013;
  rec.co2_voltage = 1.234;
}
void setup() {
  // put your setup code here, to run once:

}

void loop() {


  DINIT(115200);
  Serial1.begin(115200);
  xb.begin(Serial1);

  Serial << "Set up OK" << ENDL;

  Serial << ENDL << "A. Sending string 'Testing a string'" << ENDL;
  RF_OPEN_STRING(xb);
  xb << "Testing a string" << ENDL;
  RF_CLOSE_STRING(xb);

  Serial << ENDL << "B. Sending string 'Test: byte=5, char=a, int=5, uint8=5, int8=-5, uint16=5, int16=-5'" << ENDL;
  RF_OPEN_STRING(xb);
  xb << "Test: byte=" << (byte) 5 << ", char=" << 'a'
     << ", int=" << (int) 5
     << ", uint8=" << (uint8_t) 5
     << ", int8=" << (int8_t) - 5
     << ", uint16=" << (uint16_t) 5
     << ", int16=" << (int16_t) - 5;
  RF_CLOSE_STRING(xb);

  Serial << ENDL <<  "C. Sending the following IsaTwoRecord" << ENDL;
  initRecord(rec);

  rec.print(Serial);
  bool sent = xb.send(rec);
  if(sent){
    Serial << "Frame sent" << ENDL;
  } else {
    Serial << "Couldn't send the record" << ENDL;
  }
  
}
