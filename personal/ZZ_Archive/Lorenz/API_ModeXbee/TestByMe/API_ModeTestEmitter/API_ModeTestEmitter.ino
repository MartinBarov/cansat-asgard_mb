#include <XBee.h>

//Pin on Uno (if using one)
const int RF_RxPinOnUno = 9;
const int RF_TxPinOnUno = 11;

//Define RF Serial
# ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#  else
#  include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
# endif

XBee xbee = XBee();

//The message we want to send
uint8_t payload[] = {'Y', 'O'};

//Unicast to RF TSCV
XBeeAddress64 addr64 = XBeeAddress64(0x0013a200, 0x415e655f);

ZBTxStatusResponse txStatus = ZBTxStatusResponse();

//What is to send
ZBTxRequest zbtx = ZBTxRequest(addr64, payload, sizeof(payload));


void setup() {
  //LED to show it is working
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  //Init Serials
  Serial.begin(115200);
  RF.begin(115200);

  //Xbee setSerial
  xbee.setSerial(RF);

  //Wait for Serial to be ready
  while (!Serial);
  Serial.println("===Beginning of the Tests===");
  Serial.println("Emitter side");
  delay(5000);
}

void loop() {
  xbee.send(zbtx);
  if (xbee.readPacket(500)) {
    //Response received!
    if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
      xbee.getResponse().getZBTxStatusResponse(txStatus);

      // get the delivery status, the fifth byte
      if (txStatus.getDeliveryStatus() == SUCCESS) {
        // success.  time to celebrate
        Serial.println("Youhou it works");
      } else {
        // the remote XBee did not receive our packet. is it powered on?
        Serial.println("Oopssss error...");
      }
    }
  } else if (xbee.getResponse().isError()) {
    Serial.print("Error reading packet.  Error code: ");
    Serial.println(xbee.getResponse().getErrorCode());
  } else {
    // local XBee did not provide a timely TX Status Response -- should not happen
    Serial.println("local XBee did not provide a timely TX Status Response -- should not happen");
  }

  delay(1000);
}
