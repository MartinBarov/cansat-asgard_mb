/*
   This is a test program to test the SI4432 capabilities, using the break-out from ElectroDragon
   (https://www.electrodragon.com/w/Si4432).

   It emits fixed duration (burstDuration) bursts at a particular frequency, with a particular mdem configuration (PSK, 2kbps).
   
   It uses the RadioHead Packet Radio library which supercedes the RH_22 library.
   RadioHead documentation: http://www.airspayce.com/mikem/arduino/RadioHead/

   OPEN ISSUES ans CONNEXIONS:
      See Scanner sketch

*/

#define DEBUG_CSPU     // To activate debugging functions in DebugCSPU.h
#include "DebugCSPU.h"
#include "RH_RF22.h"
#include "elapsedMillis.h"

#define FREQ_ALL                    0
#define FREQ_PUBLIC_MOBILE_NETWORK  1
#define FREQ_433_500                2
#define FREQ_433_440                3

// *** Update behaviour hereunder ***
const uint16_t burstDuration   = 5000;     // msec. The duration of a transmission burst. 
#define FREQUENCY_RANGE FREQ_433_440   // Update to select scanning frequency settings
const float frequencyStep  = 1.0;  // MHz  The frequency step used to scan.
const uint8_t outputPower = RH_RF22_TXPOW_8DBM;
// Valid valudes:  RH_RF22_TXPOW_1DBM, RH_RF22_TXPOW_2DBM, RH_RF22_TXPOW_5DBM, RH_RF22_TXPOW_8DBM, RH_RF22_TXPOW_11DBM, RH_RF22_TXPOW_14DBM, 
//                 RH_RF22_TXPOW_17DBM, RH_RF22_TXPOW_20DBM

// *** Values hereunder should not require tuning ***
const RH_RF22::ModemConfigChoice modemConfig=RH_RF22::UnmodulatedCarrier;
const byte CS_Pin=10;                   // Digital pin used as Chip-Select line for the SS4432
const byte InterruptPin=2;              // Digital pin used by the SS4432 to trigger interruption
const bool showStatusRegister=true;    // If true, status register is printed regularly.

#if (FREQUENCY_RANGE==FREQ_ALL) 
const float minFrequency = 433.0; // MHz 433=minimum supported by SI4432, min supported by driver=240.0
const float maxFrequency = 868.0; // MHz  868=maximum supported by SI4432, max supported by driver=960.0
#elif (FREQUENCY_RANGE==FREQ_PUBLIC_MOBILE_NETWORK)
  const float minFrequency = 791.0; // MHz 
  const float maxFrequency = 821.0; // MHz 
#elif (FREQUENCY_RANGE==FREQ_433_500)
  const float minFrequency = 433.0; // MHz 
  const float maxFrequency = 500.0; // MHz 
#elif (FREQUENCY_RANGE==FREQ_433_440)
  const float minFrequency = 433.0; // MHz 
  const float maxFrequency = 440.0; // MHz 
#else#else 
#  error "Invalide FREQUENCY_RANGE value:" FREQUENCY_RANGE
#endif

// Singleton instance of the radio
RH_RF22 rf22(CS_Pin, InterruptPin);
float currentFrequency;

void printStatusRegister(bool force=false) {
  if ((!force) && (!showStatusRegister)) return;
  uint8_t reg=rf22.statusRead();
  Serial << "Status register: " << reg << ":";
  Serial.print(reg & 0b10000000 ? "RxTx FIFO overflow " : " ");
  Serial.print(reg & 0b01000000 ? "RxTx FIFO underflow " : " ");
  Serial.print(reg & 0b00100000 ? "RxTx FIFO empty " : "RxTx FIFO not empty ");
  Serial.print(reg & 0b00010000 ? "Header error " : " ");
  Serial.print(reg & 0b00001000 ? "Frequency error " : " ");
  Serial.print(reg & 0b00000100 ? "?? " : " ");
  switch(reg & 0b00000011) {
    case 0b00:
        Serial << "IDLE";
        break;
    case 0b01: 
        Serial << "RX-State";
        break;
    case 0b10:
        Serial << "TX-State";
        break;
    default:
        Serial << "**Unexpected state**";
  }
  Serial << ENDL;
  Serial.flush();
}

void setup() {
  DINIT(115200);
  Serial << "****************************" << ENDL;
  Serial << "**   RF Emitter utility   **" << ENDL;
  Serial << "****************************" << ENDL;
   
  Serial << "Waiting for RF22 initialization...";
  Serial.flush();
  // delay(3000); // UNNECESSARY wait before using the RF22: first attempt always fails. Waiting 2 sec is not alwyas enough
  
  if (!rf22.init()) {
    Serial << "RF22 init failed. Check wiring!" << ENDL;
    Serial.flush();
    exit(-1);
  }
  Serial << "RF22 init OK" << ENDL; 
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36 (2.4 kbps)
  rf22.setModemConfig(modemConfig);
  currentFrequency = minFrequency;
  rf22.setFrequency(currentFrequency);
  rf22.setModeIdle();
  Serial << "Setup OK." << ENDL;
  Serial << "Emitting from " << minFrequency << " to " << maxFrequency
         << " MHz (step = " << frequencyStep  << " Mhz)" << ENDL;
  Serial << "Using " << burstDuration << " msec bursts"  << ENDL;
  printStatusRegister(true);
  Serial << "Initialisation over" << ENDL << ENDL;
  // delay(2000); // RF22 apparently need more than 1 sec to stabilize.
  // printStatusRegister();
}

/* 
 * 
   @return True if the maximum frequency has been reached (and the scanning cycle completed), false otherwise. 
 */
bool emitOnOneFrequency() {
  bool result=false;
  
  Serial << "Emitting at " << currentFrequency << " MHz during " << burstDuration 
         << " msec..." << ENDL;
  Serial.flush();
         
  rf22.setModeTx();
  delay(1); // to make sure the status register reflects the new state
  Serial << "  "; 
  printStatusRegister();
  delay(burstDuration);
  rf22.setModeIdle();
  delay(1); // to make sure the status register reflects the new state
  Serial << "  ";
  printStatusRegister();
  Serial << "Done." << ENDL;
  
  currentFrequency += frequencyStep;
  if (currentFrequency > maxFrequency) {
    currentFrequency = minFrequency;
    result = true;
  }
  rf22.setFrequency(currentFrequency);
  rf22.setTxPower(outputPower);
  // Serial << millis() << ": frequency set to " << currentFrequency << " MHz" << ENDL;
  return result;
}


void loop() {
  
  // Emit during burstDuration, wait for user interaction to switch to next frequency.  
  if (emitOnOneFrequency()) {
    Serial << "--------- Cycle complete ---------" << ENDL << ENDL;
  }

  // Discard content of input buffer.
  while (Serial.available()) Serial.read();
  Serial << "Type 'n' (next frequency), 'r' (reset to min. frequency) or 'q' (quit)" << ENDL;
  Serial << "and press return" << ENDL;
  bool doneWaiting=false;
  while (!doneWaiting) {
    if (Serial.available()) {
      char c = Serial.read();
      switch(c) {
        case 'n':
        case 'N':
          doneWaiting=true;
          break;
        case 'q':
        case 'Q':
          Serial << "Terminating program" << ENDL;
          Serial.flush();
          exit(0);
        case 'r':
        case 'R':
          currentFrequency=minFrequency;
          rf22.setFrequency(currentFrequency);
          Serial << "Currency frequency reset to " << minFrequency << " MHz" << ENDL;
          delay(5);
          doneWaiting=true;
          break;
        case '\n':
        case '\r':
          break;
        default:
          Serial << "Unsupported command '" << c << "' (ignored)." << ENDL;
      }
    }
  } // while
}
