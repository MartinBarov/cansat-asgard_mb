
#include "BlinkingLED.h"


BlinkingLED::BlinkingLED(byte thePinNumber, unsigned long theDuration) {
  duration = theDuration;
  pinNumber = thePinNumber;
  pinMode(pinNumber, OUTPUT);
  ts = millis();
}

void BlinkingLED::run() {
  unsigned long elapsed=millis() - ts;
  if (elapsed >= duration) {
    digitalWrite(pinNumber,  !digitalRead(pinNumber) );
    ts = millis();
  }

}
