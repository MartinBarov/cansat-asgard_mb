#define DEBUG_CSPU
#include "DebugCSPU.h"


#define SYMBOL thedefinedsymbol
#define GPS_CLIENT_TIMER TC4
#define GPS_CLIENT_TIMER_NUMBER 4


#define CONCAT2(a, b) a ## b
#define CONCAT(a, b) CONCAT2(a, b)


/*
  void SYMBOL1##abc(){
  ;
  }
*/

/*
  void CONCAT2(SYMBOL, abc)(){
  Serial << "Function executed" << ENDL;
  }
*/

/*
  void CONCAT(SYMBOL, abc)() {
  Serial << "Function executed" << ENDL;
  }
*/

/*
  void CONCAT(GPS_CLIENT_TIMER, _test)() {
  Serial << "Function executed" << ENDL;
  }
*/

/*
  void CONCAT(TC, CONCAT(GPS_CLIENT_TIMER_NUMBER, _test))() {
  Serial << "Function executed" << ENDL;
  }
*/

void setup() {
  DINIT(115200);

  //thedefinedsymbolabc();
  //SYMBOLabc();
  //TC4_test();
}

void loop() {
  ;
}
