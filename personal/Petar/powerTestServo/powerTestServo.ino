#ifndef ARDUINO_ARCH_SAMD
#error "Incompatible board (SAMD21 processor required)."
#endif

#include "TorusServoWinch.h"
#include "elapsedMillis.h"

#define USE_ASSERTIONS
#define DEBUG_CSPU
#include "DebugCSPU.h"

const byte PWM_Pin = 9;
const uint16_t motorSpeedToUse = 10; // (mm/s)

TorusServoWinch servo;

void waitForUser(char msg[] = "Press any key to continue") {
  while (Serial.available() > 0) {
    Serial.read();
  }
  Serial.println(msg);
  Serial.flush();
  while (Serial.available() == 0) {
    delay(300);
  }
  Serial.read();
}

void wait20s(){
  elapsedMillis elapsed = 0;
  while(elapsed < 3000) {
    servo.run();
  }
}


void setup() {
  DINIT(115200);


  waitForUser("Press enter to start");

  delay(1000);

  servo.begin(PWM_Pin, motorSpeedToUse);
  wait20s();
  

  for(int x = 0; x < 2; x++) {
    for(int i = 30; i < 100; i+=10) {
      servo.setTarget(ServoWinch::ValueType::ropeLength, i);
      wait20s();
    }
  
    for(int i = 100; i < 160; i+=20) {
      servo.setTarget(ServoWinch::ValueType::ropeLength, i);
      wait20s();
    }
  
    servo.setTarget(ServoWinch::ValueType::ropeLength, 25);
    
    waitForUser("Next iteration");
  }

  servo.end();
}

void loop() {
  // put your main code here, to run repeatedly:

}
