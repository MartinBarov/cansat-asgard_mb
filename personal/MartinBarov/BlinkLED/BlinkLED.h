#include"Arduino.h" // liste de vocabulaire
class BlinkLED {
  public:
    bool begin(const uint8_t theLED_PinNBR, const unsigned long APeriodInMsec);
    void run();
  private:
    uint8_t LED_PinNBR;
    unsigned long lastChangeTimestamp;
    unsigned long periodInMsec;

};
