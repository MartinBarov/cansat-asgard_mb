
#include "IsaTwoConfig.h"

void setup() {
  Serial.begin(115200);
  while (!Serial) ;

  pinMode(ImagerCtrl_MasterDigitalPinNbr, OUTPUT);
  digitalWrite(ImagerCtrl_MasterDigitalPinNbr,HIGH);

  Serial << "Set Imager Ctrl line (" << ImagerCtrl_MasterDigitalPinNbr << ") to HIGH" << ENDL;
}

void loop() {
  delay(100);
}
