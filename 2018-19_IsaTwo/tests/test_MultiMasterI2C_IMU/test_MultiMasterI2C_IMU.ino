
/**
*
* Sample Multi Master I2C implementation.  Sends a button state over I2C to another
* Arduino, which flashes an LED correspinding to button state.
*
* Connections: Arduino analog pins 4 and 5 are connected between the two Arduinos,
* with a 1k pullup resistor connected to each line.  Connect a push button between
* digital pin 10 and ground, and an LED (with a resistor) to digital pin 9.
*
*/

#include <Wire.h>
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "IMU_ClientLSM9DS0.h"

IsaTwoRecord rec;
IMU_ClientLSM9DS0 imu(1);
int counter=0;

void setup() {
 Serial.begin(115200);
 while (!Serial) ;

 Serial.print("Multi-Master I2C test using IMU");
 
  Wire.begin();
  imu.begin();
}

void loop() {
  counter++;
 rec.clear();
 
 imu.readData(rec);
 Serial <<counter << " . " ;
 delay(500);
}
