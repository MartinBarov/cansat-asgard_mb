
constexpr byte CtrlLine = 10;

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  Serial.print("Reading Imager CtrlLine =");
  Serial.println(CtrlLine);
  
  pinMode(CtrlLine, INPUT);
}

void loop() {
  if (digitalRead(CtrlLine) == LOW) Serial.println("LOW");
  else Serial.println("HIGH");
  delay(1000);

}
