/*
 * CansatHW_Scanner_Continuous
 * 
 * This debugging utility just repeatedly creates a HardwareScanner and prints diagnostic on the Serial output.
 * in order to detect problems in SDL/SDA pull-up and other connection issues. 
 * 
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatHW_Scanner.h" // Include this first to have libraries linked in the right order. 
#include "CansatConfig.h"

 void setup() {
  Serial.begin(115200);
  while (!Serial) ;

  Serial.println("Repetitively running Cansat Hardware Scanner...");
  Serial.println("(Remember the I2C bus scanning will take a *very* long time on some boards");
  Serial.println(" if pull-ups are not installed on the SCL and SDA lines)");
  Serial << F("NB: the Cansat HW Scanner detects whatever can be detected, and checks") << ENDL
         << F("    the configuration against the specification of CansatConfig.h")<< ENDL;
  Serial.flush();

  Serial << ENDL << "Initializing CansatHW_Scanner with default values i.e values from CansatConfig.h..." << ENDL;
  
}

void loop() {
  CansatHW_Scanner hw;
  Serial << F("Collecting information...") << ENDL;
  hw.init();
  Serial << F("Full diagnostic:") << ENDL;
  hw.printFullDiagnostic(Serial);
  Serial << "======================================================" << ENDL;
}
