/*
 * HardwareScanner
 * 
 * This utility just creates a HardwareScanner and prints diagnostic on the Serial output.
 * Only information that can be automatically discovered is shown. 
 * 
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "HardwareScanner.h" // Include this first to have libraries linked in the right order. 

 void setup() {
  Serial.begin(115200);
  while (!Serial) ;

  Serial.println("Running Hardware Scanner...");
  Serial.println("(Remember the I2C bus scanning will take a *very* long time on some boards");
  Serial.println(" if pull-ups are not installed on the SCL and SDA lines)");
  Serial << F("NB: the HardwareScanner just detects whatever can be detected, but does NOT check") << ENDL
         << F("    the configuration against the specification of CansatConfig.h")<< ENDL;
  Serial << F("    Use CansatHW_Scanner utility to do that!") << ENDL;
  Serial.flush();

  Serial << ENDL << F("Initializing an HardwareScanner with default values...") << ENDL;
  HardwareScanner hw;
  Serial << F("Collecting information...") << ENDL;
  hw.init();
  Serial << F("Full diagnostic:") << ENDL;
  hw.printFullDiagnostic(Serial);

  Serial << ENDL << F("End of job.") << ENDL;
}

void loop() {
  // put your main code here, to run repeatedly:

}
